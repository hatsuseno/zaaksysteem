package Zaaksysteem::Backend::Sysin;
use Moose;

use BTTW::Tools;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Backend::Sysin - A base class for Zaaksysteem::Backend::Sysin::XXX::Model classes.

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 interface

=cut

has interface => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Model::DB::Interface',
    required => 1,
);

=head2 schema

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    lazy     => 1,
    default  => sub {
        my $self = shift;
        return $self->interface->result_source->schema;
    }
);

has attribute_mapping => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $self = shift;
        return $self->interface->get_attribute_mapping;
    }
);

=head1 METHODS

=head2 new_from_interface

Create a new object of L<Zaaksysteem::Backend::Sysin> based on the interface. You probably need to overwrite this function

=cut

sub new_from_interface {
    my ($self, $interface) = @_;

    return $self->new(
        interface => $interface,
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
