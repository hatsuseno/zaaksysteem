package Zaaksysteem::Queue;
use Moose;

use BTTW::Tools;
use JSON::XS qw[];
use Moose::Util::TypeConstraints qw[enum];

with 'MooseX::Log::Log4perl';
use Zaaksysteem::Types qw(UUID);

=head1 NAME

Zaaksysteem::Queue - A wrapper module around the queue item mechanism

=head1 SYNOPSIS

    use Zaaksysteem::Queue;
    my $queue = Zaaksysteem::Queue->new(
        schema => $schema;
    );

    $queue->create_item();

=head1 ATTRIBUTES

=head2 schema

An L<Zaaksysteem::Schema> object, required

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 lock_id

Contains the current lock identifier (if locked).

Exposes C<has_lock> and C<clear_lock> delegates.

=cut

has lock_id => (
    is        => 'rw',
    isa       => 'Int',
    predicate => 'has_lock',
    clearer   => 'clear_lock',
);

=head2 json

Custom L<JSON::XS> instance for item data. Since this model checks if
requested queue item already exists a canonical representation of the data is
required, which this attribute provides through a builder.

=cut

has json => (
    is => 'rw',
    isa => 'JSON::XS',
    default => sub {
        return JSON::XS->new->canonical(1);
    }
);

sub lock {
    my $self = shift;
    if ($self->has_lock) {
        $self->log->trace(sprintf("Setting lock %d", $self->lock_id));
        $self->schema->lock($self->lock_id);
    }
    return 1;
}

sub unlock {
    my $self = shift;
    if ($self->has_lock) {
        $self->log->trace(sprintf("Unlocking lock %d", $self->lock_id));
        $self->schema->unlock($self->lock_id);
    }
    return 1;
}

=head1 METHODS

=head2 create_item

Creates a queue item for you and it checks if there are already similar items in the queue with the same label, data and type.

=cut

define_profile create_item => (
    required => {
        label => 'Str',
        type  => 'Str',
        data  => 'HashRef',
    },
    optional => {
        status    => enum([qw[pending waiting]]),
        object_id => UUID,
        ignore_existing => 'Bool'
    },
    defaults => {
        status => 'pending',
        ignore_existing => 0
    }
);

sub create_item {
    my $self = shift;
    my $options = assert_profile({@_})->valid;

    my $label     = $options->{label};
    my $type      = $options->{type};
    my $data      = $options->{data};
    my $status    = $options->{status};
    my $object_id = $options->{object_id};

    $self->log->trace(
        sprintf(
            "Creating queue item with label: '%s', type: '%s' data: %s)",
            $label, $type, dump_terse($data)
        )
    );

    # This uses the same ordering of overriding hash keys as the create_item
    # call below ends up with.
    my $queue_data = try {
        return $self->json->encode($data);
    } catch {
        throw(
            'queue/create_item/item_data_serialization_failed',
            sprintf('Failed to encode queue item data: %s', $_),
            $data
        );
    };

    $self->lock();

    unless ($options->{ ignore_existing }) {
        my $existing = $self->schema->resultset('Queue')->search(
            {
                status => $status,
                type   => $type,
                label  => $label,
                data   => $queue_data,
            }
        )->first;

        if ($existing) {
            $self->unlock();

            $self->log->info(sprintf(
                'Not re-creating %s queue item "%s", already exists',
                $type,
                dump_terse($queue_data)
            ));

            return;
        }
    }

    my $item = try {
        return $self->schema->resultset('Queue')->create_item($type, {
            label => $label,
            data => $data,
            status => $status,
            $object_id ? (object_id => $object_id) : (),
        });
    } catch {
        $self->unlock;

        throw(
            'queue/create_item/create_failed',
            sprintf('Failed to create %s queue item: %s', $_),
            $data
        );
    };

    $self->unlock;

    return $item;
}

=head2 add_to_queue

Add a qitem to the queue item broadcast mechanism.

=cut

sub add_to_queue {
    my $self = shift;
    my $item = shift;
    push(@{$self->schema->default_resultset_attributes->{queue_items}}, $item);
}



__PACKAGE__->meta->make_immutable;

__END__


=head1 AUTHOR

Wesley Schwengle

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
