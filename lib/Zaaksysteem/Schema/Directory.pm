use utf8;
package Zaaksysteem::Schema::Directory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Directory

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<directory>

=cut

__PACKAGE__->table("directory");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'directory_id_seq'

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 case_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 original_name

  data_type: 'text'
  is_nullable: 0

=head2 path

  data_type: 'integer[]'
  default_value: ARRAY[]::integer[]
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "directory_id_seq",
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "case_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "original_name",
  { data_type => "text", is_nullable => 0 },
  "path",
  {
    data_type     => "integer[]",
    default_value => \"ARRAY[]::integer[]",
    is_nullable   => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<name_case_id>

=over 4

=item * L</name>

=item * L</case_id>

=back

=cut

__PACKAGE__->add_unique_constraint("name_case_id", ["name", "case_id"]);

=head1 RELATIONS

=head2 case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("case_id", "Zaaksysteem::Schema::Zaak", { id => "case_id" });

=head2 files

Type: has_many

Related object: L<Zaaksysteem::Schema::File>

=cut

__PACKAGE__->has_many(
  "files",
  "Zaaksysteem::Schema::File",
  { "foreign.directory_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-06-15 14:24:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/M/wpV4Xifhdh9bkkuYoyA

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Directory::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::Directory::Component
    +Zaaksysteem::Helper::ToJSON
/);

__PACKAGE__->belongs_to("case", "Zaaksysteem::Schema::Zaak", { id => "case_id" });

# You can replace this text with custom code or comments, and it will be preserved on regeneration

__PACKAGE__->has_many(
    'files',
    'Zaaksysteem::Schema::File',
    { 'foreign.directory_id' => 'self.id' },
    { cascade_delete => 0 }
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

