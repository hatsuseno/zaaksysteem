package Zaaksysteem::Zaken::ComponentBag;

use strict;
use warnings;

use base qw/DBIx::Class/;

sub update_bag {
    my  $self   = shift;
    my  $params = shift;

    if ($params) {
        return 1 unless (
            UNIVERSAL::isa($params, 'HASH') &&
            scalar(%{ $params })
        );
    } else {
        $params = $self;
    }

    ### Get default resultset
    if (my $fixed_row = $self->result_source->schema->resultset('BagNummeraanduiding')->_retrieve_zaakbag_data(
            $params
        )
    ) {
        $self->update($fixed_row);
    }
}

sub columns_bag {
    my $self        = shift;
    my %columns_bag;

    my %cols        = $self->get_columns;
    while (my ($colname, $colvalue) = each %cols) {
        next if grep { $colname eq $_ } qw/zaak_id pid id/;

        $colname    =~ s/bag_//;
        $colname    =~ s/_id//;

        $columns_bag{$colname} = $colvalue;
    }

    return %columns_bag;
}

sub _verify_bagdata {
    my $self    = shift;

    ### Get default resultset
    return $self->result_source->schema->resultset('BagNummeraanduiding')->_retrieve_zaakbag_data(
        $self,
        @_
    );
}

sub maps_adres {
    my $self    = shift;

    #return '' unless $self->bag_nummeraanduiding_id;

    ### Get gegevens_model
    my ($nummeraanduiding, $openbareruimte);

    if ($self->bag_nummeraanduiding_id) {
        $nummeraanduiding    = $self->result_source->schema->resultset('BagNummeraanduiding')->search(
            {
                identificatie   => $self->bag_nummeraanduiding_id
            }
        )->first or return '';

        $openbareruimte     = $nummeraanduiding->openbareruimte;
    } elsif ($self->bag_openbareruimte_id) {
        $openbareruimte     = $self->result_source->schema->resultset('BagOpenbareruimte')->search(
            {
                identificatie   => $self->bag_openbareruimte_id
            }
        )->first or return '';
    }

    #return '' unless $nummeraanduiding->openbareruimte;

    my $address = 'Netherlands, '
        . $openbareruimte->woonplaats->naam . ', '
        . $openbareruimte->naam;

    if ($nummeraanduiding) {
        $address .= ', ' . $nummeraanduiding->huisnummer;
    }

    return $address;
}

=head2 verblijfsobject_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_verblijfobject_id.

=cut

sub verblijfsobject_id {
    my $self = shift;
    return unless $self->bag_verblijfsobject_id;
    return "verblijfsobject-" . $self->bag_verblijfsobject_id;
}

=head2 openbareruimte_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_verblijfobject_id.

=cut

sub openbareruimte_id {
    my $self = shift;
    return unless $self->bag_openbareruimte_id;
    return "openbareruimte-" . $self->bag_openbareruimte_id;
}

=head2 nummeraanduiding_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_nummeraanduiding_id.

=cut

sub nummeraanduiding_id {
    my $self = shift;
    return unless $self->bag_nummeraanduiding_id;
    return "nummeraanduiding-" . $self->bag_nummeraanduiding_id;
}

=head2 pand_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_pand_id.

=cut

sub pand_id {
    my $self = shift;
    return unless $self->bag_pand_id;
    return "pand-" . $self->bag_pand_id;
}

=head2 standplaats_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_standplaats_id.

=cut

sub standplaats_id {
    my $self = shift;
    return unless $self->bag_standplaats_id;
    return "standplaats-" . $self->bag_standplaats_id;
}

=head2 ligplaats_id

Return the "bag id-style" string for this C<ZaakBag>'s bag_ligplaats_id.

=cut

sub ligplaats_id {
    my $self = shift;
    return unless $self->bag_ligplaats_id;
    return "ligplaats-" . $self->bag_ligplaats_id;
}

sub insert {
    my $self    = shift;

    my $rv      = $self->next::method(@_);

    if (ref($self->zaak_id)) {
        $self->zaak_id->touch;
    }

    return $rv;
};

sub update {
    my $self    = shift;

    my $rv      = $self->next::method(@_);

    if (ref($self->zaak_id)) {
        $self->zaak_id->touch;
    }

    return $rv;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 columns_bag

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 maps_adres

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

=head2 update_bag

TODO: Fix the POD

=cut

