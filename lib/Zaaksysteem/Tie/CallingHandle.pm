package Zaaksysteem::Tie::CallingHandle;
use base 'Tie::Handle';

use Class::Accessor "moose-like";
use Symbol;

=head1 NAME

Zaaksysteem::Tie::CallingHandle - Call custom code for every write() on a handle

=head1 DESCRIPTION

Some modules don't use L<IO::Handle> and its OO interface, but use the "old"
built-in functions.

Using this module, it becomes possible to "catch" whatever such a module is
writing to the handle, and do your own processing/handling on it, without first
writing the data to a temporary file or memory buffer.

This class extends L<Tie::Handle>.

=head1 SYNOPSIS

    my $outside = SomeClass->new();
    my $handle = Zaaksysteem::Tie::CallingHandle->create(
        write_cb => sub { $outside->some_method($_[0]); },
    );

    print $handle "This calls \$outside->some_method(\"This ... etc\")\n";
    syswrite $handle "And so does this\n";

=head1 ATTRIBUTES

=head2 write_cb(scalar, length, offset)

Callback that's called every time L<write()> is called on the filehandle.

This is a required attribute.

=cut

has write_cb => (is => 'ro');

=head1 METHODS

=head2 create(%new_arguments)

Create new C<Zaaksysteem::Tie::CallingHandle> instance.

%arguments should contain the a "write_cb" key, with the callback code
reference as its value.

=cut

sub create {
    my $class = shift;

    my $handle = gensym();
    tie *{$handle}, $class, @_;

    return $handle;
}

sub TIEHANDLE {
    my $class = shift;

    # Don't call $class->new(), Tie::Handle has it call ->TIEHANDLE.. which
    # leads to a loop.
    my $self = {@_};
    return bless($self, $class);
}

sub WRITE {
    my $self = shift;

    return $self->write_cb()->(@_);
}

sub BINMODE {
    my $self = shift;
    return 1;
}

sub FILENO {
    my $self = shift;
    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
