package Zaaksysteem::Controller::API::v1::Status;

use Moose;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

use Zaaksysteem::API::v1::Status;

=head1 NAME

Zaaksysteem::Controller::API::v1::Status - API v1 Status controller

=head1 DESCRIPTION

This minimalist controller serves as both a "by example" API v1 controller
implementation, as well as hosting a stable endpoint which can be used in
documentation and other examples for API interactions.

=head1 ATTRIBUTES

=head2 api_capabilities

This attribute declares the controllers accessibility scope.

=cut

has api_capabilities => (
    is => 'ro',
    default => sub { return [qw[extern]] }
);

=head1 ACTIONS

=head2 status

Returns the status of the Zaaksysteem instance.

=cut

sub status : Chained('/api/v1/base') : PathPart('status') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ result } = Zaaksysteem::API::v1::Status->new(
        version => $c->config->{ ZS_VERSION }
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
