package Zaaksysteem::Controller::API::v1::General::LegalEntitiesTypes;
use Moose;
use DateTime;

use BTTW::Tools;
use Zaaksysteem::Constants qw(GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES);
use Zaaksysteem::Object::ConstantTables qw/LEGAL_ENTITY_TYPE_TABLE/;
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Object::Types::LegalEntityType;
use List::Util qw/first/;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has api_capabilities => (
    is          => 'ro',
    default     => sub { return [qw/extern intern allow_pip/] }
);

has '+namespace' => (
    default => 'legal_entity_type'
);

=head1 NAME

Zaaksysteem::Controller::API::v1::General::LegalEntities - API v1 controller for legal entitities

=head1 DESCRIPTION

This controller returns the 'rechtsvormen' or legal entitities which are allowed in Zaaksysteem

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/general/legal_entity_types> URI namespace.

=cut

sub base : Chained('/api/v1/general/base') : PathPart('legal_entity_types') : CaptureArgs(0) : Scope('general') {
    my ($self, $c)      = @_;

    $self->get_all_legal_entity_types($c);
}

=head2 list

=head3 URL Path

C</api/v1/general/legal_entity_type>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->list_set($c);
}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $internal_id) = @_;

    my $entity  = first { $_->{code} == int($internal_id) } @{ LEGAL_ENTITY_TYPE_TABLE() };

    if ($entity) {
        $c->stash->{$self->namespace} = Zaaksysteem::Object::Types::LegalEntityType->new(%$entity);
    }
    else {
        throw(
            'api/v1/general/legal_entity_type/not_found',
            "Unable to find legal entity type with id '$internal_id'",
        );
    }
}

=head2 get

=head3 URL Path

C</api/v1/general/legal_entity_type/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->get_object($c);
}


=head2 create

This action is not implemented

=head3 URL Path

C</api/v1/general/legal_entity_type/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    throw(
        'api/v1/general/legal_entities/create/not_allowed',
        'Creation of legal entities is not allowed',
    );

}

=head2 delete

This action is not implemented

=head3 URL Path

C</api/v1/general/legal_entity_type/UUID/delete>

=cut

sub delete : Chained('instance_base') : PathPart('delete') : Args(0) : RW {
    my ($self, $c)      = @_;

    $self->assert_post($c);

    throw(
        'api/v1/general/legal_entities/delete/not_allowed',
        'Deletion of legal entities is not allowed',
    );
}

=head1 METHODS

=head2 get_all_legal_entity_types

Get all the legal entitities from Zaaksysteem

=cut

sub get_all_legal_entity_types {
    my ($self, $c)      = @_;

    my $entities = LEGAL_ENTITY_TYPE_TABLE();

    my @o;
    foreach (sort { $a->{label} cmp $b->{label} } @$entities) {
        next unless $_->{active};

        push(@o, Zaaksysteem::Object::Types::LegalEntityType->new(%$_));
    }

    my $set = Zaaksysteem::API::v1::ArraySet->new(
        content => \@o,
        allow_rows_per_page => 500,
    );

    $c->stash->{set} = $set;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
