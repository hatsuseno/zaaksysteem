package Zaaksysteem::Controller::API::v1::Subject;

use Moose;
use Moose::Util::TypeConstraints qw[enum union];

use DateTime;
use JSON qw[decode_json];

use Zaaksysteem::API::v1::ResultSet;
use Zaaksysteem::BR::Subject;
use Zaaksysteem::Constants::Users qw(API REGULAR);
use Zaaksysteem::Object::Types::Address;
use Zaaksysteem::Object::Types::CountryCode;
use BTTW::Tools::MergeObject qw(merge_moose_obj);
use BTTW::Tools;
use Zaaksysteem::Types qw(SubjectType UUID BSN);


BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/extern intern/] }
);

=head1 NAME

Zaaksysteem::Controller::API::V1::Subject - APIv1 controller for subjects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/subject>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Subject>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Subject>

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/base') : PathPart('subject') : CaptureArgs(0) : Scope('subject') {}

=head2 search

/api/v1/subject

=cut

sub search : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    return $self->_process_search_results($c);
}

=head2 remote_search

/api/v1/subject/remote_search

=cut

sub remote_search : Chained('base') : PathPart('remote_search') : RO {
    my ($self, $c, @args) = @_;

    throw(
        'api/v1/subject/forbidden',
        'Searching of remote objects only allowed for logged in users'
    ) unless $c->user_exists;

    my $config_interface_id = $args[0];

    if (!$config_interface_id) {
        my $module  = $c->req->params->{query}->{match}->{subject_type} eq 'company' ? 'overheidio' : 'stufconfig';

        my @configs = $c->model('DB::Interface')->search_active(
            {module => $module},
            {order  => 'id'}
        )->all;

        if (@configs == 1) {
            $config_interface_id = $configs[0]->id;
        }
    }

    throw(
        'api/v1/subject/remote_import/interface_id_incorrect',
        'Interface ID should be a number'
    ) unless $config_interface_id =~ /^\d+$/;

    return $self->_process_search_results(
        $c,
        remote_search       => 1,
        config_interface_id => $config_interface_id,
    );
}

=head2 remote_import

/api/v1/subject/remote_import

=cut

sub remote_import : Chained('base') : PathPart('remote_import') : RO {
    my ($self, $c, @args) = @_;

    ### Prevent creation of subjects by unknown users
    throw(
        'api/v1/subject/forbidden',
        'Importing of remote subjects only allowed for logged in users'
    ) unless $c->user_exists;

    my $subject = $self->_convert_to_clean_object($c->req->params, drop_dates => 1);

    throw(
        'api/v1/subject/remote_import/incorrect_subject_type',
        'Remote import is only allowed for subject type "person"'
    ) unless ($subject->{subject_type} =~ /^(person|company)$/);

    my $config_interface_id = $args[0];

    if ($subject->{subject_type} eq 'company' && (!$config_interface_id || $config_interface_id !~ /^\d+$/)) {
        $config_interface_id = $c->model('DB::Interface')->search_active(
            { module => 'overheidio' },
            { order  => 'id' }
        )->get_column('id')->first;
    }

    throw(
        'api/v1/subject/remote_import/interface_id_incorrect',
        'Interface ID should be a number'
    ) unless $config_interface_id =~ /^\d+$/;

    my $bridge              = $self->bridge(
        $c,
        remote_search => ($subject->{subject_type} eq 'person' ? 'stuf' : 'openkvk'),
        config_interface_id => $config_interface_id,
    );

    my $object              = $bridge->remote_import($subject);

    $c->stash->{result}     = $object;
}


=head2 instance_base

Preperation for /api/v1/subject/UUID


=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    throw('api/v1/subject/invalid_uuid','Invalid UUID given') unless UUID->check($uuid);

    my $bridge              = $self->bridge($c);
    $c->stash->{subject}    = $bridge->find($uuid);

    throw('api/v1/subject/uuid_not_found', 'No subject found by given UUID') unless $c->stash->{subject};
}

=head2 get

/api/v1/subject/UUID

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{result} = $c->stash->{subject};
}

=head2 create

=head3 URL Path

C</api/v1/subject/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    $c->assert_user(API|REGULAR);

    my $bridge      = $self->bridge($c);

    my $object      = $bridge->object_from_params($c->req->params);

    $c->model('DB')->txn_do(sub {
        $bridge->save($object);
    });

    $c->stash->{result} = $object;
}

=head2 update

=head3 URL Path

C</api/v1/subject/create>

=cut

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    $c->assert_user(API|REGULAR);

    my $subject = $c->stash->{ subject };
    my $type    = $subject->subject_type;

    my $params = $c->req->params;

    my $bridge = $self->bridge($c);

    if ($subject->has_external_subscription)  {

        my %updates = map { $_ => $params->{subject}{$_} } qw(
            email_address
            phone_number
            mobile_phone_number
        );

        $subject->subject(merge_moose_obj($subject->subject, \%updates));

    }
    else {

        if ($type eq 'person') {
            $self->_assert_person($subject, $params, $bridge);
        }
        elsif ($type eq 'company') {
            $self->_assert_company($subject, $params, $bridge);
        }
        elsif ($type eq 'employee') {
            $self->_assert_employee($subject, $params, $bridge);
        }

        $subject = $self->_update_address($subject, $params);

        $subject->subject(merge_moose_obj($subject->subject, $params->{subject}));

    }

    $c->model('DB')->txn_do(sub {
        $subject = $bridge->save($subject);
    });

    $c->stash->{result} = $subject;
}

sub _assert_employee {
    my ($self, $subject, $params, $bridge) = @_;

    # TODO: Actually assert
    return 1;
}

sub _assert_company {
    my ($self, $subject, $params, $bridge) = @_;

    my $kvk = $params->{subject}{coc_number};
    my $vestiging = $params->{subject}{coc_location_number};

    if ($kvk || $vestiging) {

        #if ($kvk && !BSN->check($kvk)) {
        #    throw('subject/update/kvk/elfproef', "Unable to modify the KVK, it isn't elfproef");
        #}

        my $found = $bridge->search({
            subject_type => $subject->subject_type,
            subject => {
                $kvk ? ( coc_number => $kvk ) : (),
                $vestiging ? ( coc_location_number => $vestiging ) : (),
            }
        });

        my $entry = $found->next;
        if ($found->next) {
            throw('subject/update/kvk/exists/duplicate', "Unable to modify the KVK, duplicate entries found");
        }
        elsif ($entry && $entry->id ne $subject->id) {
            throw('subject/update/kvk/exists', "Unable to modify the KVK, it already exists");
        }

    }
    return 1;
}

sub _assert_person {
    my ($self, $subject, $params, $bridge) = @_;

    if (my $bsn = $params->{subject}{personal_number}) {

        if (!BSN->check($bsn)) {
            throw('subject/update/bsn/elfproef', "Unable to modify the BSN, it isn't elfproef");
        }

        my $found = $bridge->search({
            subject_type    => $subject->subject_type,
            subject => {
                personal_number => $bsn,
            }
        });

        my $entry = $found->next;
        if ($found->next) {
            throw('subject/update/bsn/exists/duplicate', "Unable to modify the BSN, duplicate entries found");
        }
        elsif ($entry && $entry->id ne $subject->id) {
            throw('subject/update/bsn/exists', "Unable to modify the BSN, it already exists");
        }

    }
    return 1;
}

sub _update_address {
    my ($self, $subject, $params) = @_;

    if ($subject->subject_type !~ /^(?:company|person)$/) {
        return;
    }

    foreach my $a (qw(address_residence address_correspondence)) {
        my $current = $subject->subject->$a;

        if (exists $params->{subject}{$a}) {
            my $new = $params->{subject}{$a};

            if ($new && $current) {

                if (my $country = delete $new->{country}) {
                    $current->country(Zaaksysteem::Object::Types::CountryCode->new_from_code($country->{dutch_code}));
                }

                if ($new) {
                    $subject->subject->$a(merge_moose_obj($current, $new));
                }

            }
            elsif ($new) {
                my $country = delete $new->{country};
                $subject->subject->$a(Zaaksysteem::Object::Types::Address->new(
                    %$new,
                    country => Zaaksysteem::Object::Types::CountryCode->new_from_code(
                        $country->{dutch_code}),
                ));
            }
            else {
                my $method = "clear_$a";
                $subject->subject->$method();
            }
        }
        delete $params->{subject}{$a};
    }
    return $subject;
}

=head2 trigger_inspect_event

Triggers the creation of a subject inspection event. This feature can be used
trace/audit employees for specific data.

B<THIS ACTION IS PART OF AN UNSTABLE FEATURE DEFINITION AND MUST NOT BE USED>.

=head3 URL path

C</api/v1/subject/[UUID]/trigger_inspect_event>

=cut

sub trigger_inspect_event : Chained('instance_base') : PathPart('trigger_inspect_event') : Args(0) : RO {
    my ($self, $c) = @_;

    my $params = $c->req->params;
    my $subject = $c->stash->{ subject };

    $params->{ name } = $subject->display_name;
    $params->{ _betrokkene_identifier } = $subject->old_subject_identifier;

    $c->model('DB::Logging')->trigger('subject/inspect', {
        component => 'betrokkene',
        component_id => $subject->subject->_table_id,
        created_for => $subject->old_subject_identifier,
        data => $params
    });

    $c->stash->{ result } = {
        type => 'message',
        reference => undef,
        instance => {
            message => 'Event created'
        }
    };
}

=head1 PRIVATE METHODS

=head2 bridge

Returns the Subject Bridge

=cut


sub bridge {
    my $self = shift;
    my $c    = shift;
    my $config_interface_id = $c->req->params->{'config_interface_id'};

    if ($config_interface_id && $config_interface_id !~ /^\d+$/) {
        undef $config_interface_id;
    }

    return $c->model(
        'BR::Subject',
        {
            $config_interface_id ? ( config_interface_id => $config_interface_id ) : (),
            @_,
            $c->user ? (user => $c->user) : (),
        }
    );
}

=head2 _get_search_params_from_dsl

    {
        query   => {
            match   => {
                'subject_type'              => 'personal',
                'subject.personal_number'   =>  '54568788',
            }
        }
    }

Turns a "elasticsearch"-style hash into a query for our subject bridge

=cut

sub _get_search_params_from_dsl {
    my ($self, $params) = @_;

    throw(
        'api/v1/subject/invalid_dsl',
        'Invalid search query, supply "match" as a key-value object'
    ) unless ($params->{match} && ref $params->{match} eq 'HASH');

    my %params;
    for my $key (keys %{ $params->{match} }) {
        $params{$key} = $params->{match}->{$key};
    }

    return \%params;
}

=head2 _process_search_results

    $self->_process_search_results($c, remote_search => 'stuf');
    $self->_process_search_results($c);

=cut

sub _process_search_results {
    my ($self, $c, %opts) = @_;

    my $search_params       = $self->_get_search_params_from_dsl($c->req->params->{query});

    if ($opts{remote_search}) {
        if ($search_params->{subject_type} && $search_params->{subject_type} eq 'person') {
            $opts{remote_search} = 'stuf';
        } elsif ($search_params->{subject_type} && $search_params->{subject_type} eq 'company') {
            $opts{remote_search} = 'openkvk';
        } else {
            throw(
                'api/v1/subject/remote_search/no_remote_possible',
                'Remote searching only possible for companies and persons'
            );
        }
    }

    my $bridge              = $self->bridge($c, %opts);

    if ($opts{remote_search}) {
        my @rs                  = $bridge->search($search_params);

        $c->stash->{set}        = Zaaksysteem::API::v1::ArraySet->new(
            content => \@rs,
        );
    } else {
        my $rs                  = $bridge->search($search_params);

        $c->stash->{set}        = Zaaksysteem::API::v1::ResultSet->new(
            iterator => $rs,
        );
    }

    $c->stash->{result}     = $c->stash->{set}->init_paging($c->req);
}

=head2 _convert_to_clean_object

    # Transforms:
    {
        type      => 'subject',
        reference => 'ab89a7b-97a8b9ab7-79a07a0b-8797897',
        instance => {
            subject => {
                instance => {
                    personal_number => 123456789,
                    surname         => 'Fuego',
                }
                type     => 'person',
                reference => 'ab89a7b-97a8b9ab7-79a07a0b-8896728342',
            }
            subject_type => 'person'
        }
    }

    # Into
    # TODO: Transfor reference to "id"
    {
        subject => {
            personal_number => 123456789,
            surname         => 'Fuego',
        }
        subject_type => 'person'
    }

Will transform a typical API-v1 structure in a plain structure which can be handled by our bridge.

=cut

sub _convert_to_clean_object {
    my $self    = shift;
    my $param   = shift;
    my %opts    = @_;

    ### Convert arrays
    if (ref $param eq 'ARRAY') {
        return [ map { $self->_convert_to_clean_object($_, @_) } @$param ];
    }

    ### Convert special hashes
    return $param unless ref $param eq 'HASH';

    if (defined $param->{type} && $param->{type} eq 'set' && $param->{instance}->{rows}) {
        return [ grep { $self->_convert_to_clean_object($_->{instance}, @_) } @{ $param->{instance}->{rows} } ];
    }

    if (defined $param->{type} && exists $param->{reference}) {
        return unless $param->{instance};

        return $self->_convert_to_clean_object($param->{instance}, @_);
    }

    my @keys;
    if ($opts{drop_dates}) {
        @keys = grep { $_ !~ /^(?:date_created|date_modified)$/ } keys %$param;
    } else {
        @keys = keys %$param;
    }

    return { map { $_ => $self->_convert_to_clean_object($param->{$_}, @_) } @keys };
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
