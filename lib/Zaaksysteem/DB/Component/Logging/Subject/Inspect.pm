package Zaaksysteem::DB::Component::Logging::Subject::Inspect;

use Moose::Role;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Subject::Inspect - Handler for
C<subject/inspect> events.

=head1 METHODS

=head2 onderwerp

Generates a human readable summary of the event.

=cut

sub onderwerp {
    my $self = shift;

    my $field_msg = '';

    if ($self->data->{ field }) {
        $field_msg = sprintf(', veld "%s"', $self->data->{ field });
    }

    return sprintf(
        'Inzage verleend op betrokkene "%s"%s.',
        $self->data->{ name } // '<onbekend>',
        $field_msg
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
