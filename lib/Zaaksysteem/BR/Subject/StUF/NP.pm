package Zaaksysteem::BR::Subject::StUF::NP;
use Moose::Role;

use BTTW::Tools;
use List::Util qw/first/;

=head1 NAME

Zaaksysteem::BR::Subject::StUFBG0310::STUFNPS - Bridge helpers for module STUFNPS

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge for inflating results from StUF-questions.

=head1 ATTRIBUTES

=head2 config_interface_id

StUF config interface ID

=cut

has config_interface_id => (
    is       => 'ro',
    required => 0,
    isa      => 'Int',
);

=head1 METHODS

=head2 search

See L<Zaaksysteem::BR::Subject#search> for usage information

=cut

around 'search' => sub {
    my $method      = shift;
    my $self        = shift;
    my ($params)    = @_;

    return $self->$method(@_) if !$self->_is_remote_nps($params);

    my $interface = $self->_assert_stuf_interface($params);

    my %cleanparams = map { my $ckey = $_; $ckey =~ s/^subject\.//; $ckey => $params->{$_} } keys %$params;

    $self->_log_searched(\%cleanparams);

    delete($cleanparams{subject_type});

    $cleanparams{config_interface_id} = $self->config_interface_id;

    my $transaction         = $interface->process_trigger('search_nps', \%cleanparams);
    my $result              = $transaction->get_processor_params->{result};

    if ($transaction->get_processor_params->{error}) {
        throw(
            "br/subject/search/remote_failure",
            "Remote error: " . $transaction->get_processor_params->{error}->{message}
        );
    }

    ### We really want a new Iterator class which supports arrays. Where we can call "next,fist,search" etc
    ### on. For now, only allow list context
    if (!wantarray()) {
        throw('br/subject/search_remote', 'Error: remote searching requires list context');
    }

    my @objects;
    for my $person (@$result) {
        push(@objects, $self->object_from_params($person));
    }

    return @objects;
};

sub _log_searched {
    my ($self, $params) = @_;

    $self->schema->resultset('Logging')->trigger(
        'stuf/remote_search',
        {
            component => 'betrokkene',
            data => {
                username => ($self->user ? $self->user->username : undef),
                params   => $params,
            }
        }
    );
}

=head2 remote_import

See L<Zaaksysteem::BR::Subject#remote_import> for usage information

=cut

# Set to sub for mocking purposes
sub _get_np_by_bsn {
    my ($self, $bsn) = @_;

    try {
        return $self->schema->resultset('NatuurlijkPersoon')->get_by_bsn($bsn, 'resurrect');
    }
    catch {
        # We know this will throw an error when we cannot find a BSN in
        # our database
        return;
    };
}

=head2 _merge_existing_into_new

Merge the new person into the old one so we can update the person with the
latest and greatest information from the BRP.

This function also checks if the configuration interface C<STUFCONFIG> is the same
for the external subscription. If this does not match an error is thrown.

=cut

sub _assert_config_interface_on_subscription {
    my ($self, $s) = @_;

    if ($s->has_external_subscription) {
        my $config_interface = $self->schema->resultset('Interface')->find($self->config_interface_id);
        my $uuid = $config_interface->uuid;

        if ($s->external_subscription->config_interface_uuid ne $uuid) {
            throw(
                'subject/stuf/remote_import/config_interface/mismatch',
                sprintf(
                    "Unable to import %d due to different STUFCONFIG interface [%s / %s] ",
                    $s->subject->personal_number,
                    $s->external_subscription
                        ->config_interface_uuid,
                    $uuid
                )
            );
        }
        return $s->external_subscription;
    }
    $self->log->warn("The old entry does not have an object subcription");
    return undef;
}


sub _merge_existing_into_new {
    my $self = shift;
    my $object = shift;

    my $np = $self->_get_np_by_bsn($object->subject->personal_number);

    return $object if !$np;

    my $np_object = $np->as_object;

    $object->id($np_object->id);
    $object->subject->id($np_object->subject->id);

    if ($np_object->has_external_subscription) {
        my $os = $self->_assert_config_interface_on_subscription($np_object);
        $object->external_subscription($os) if $os;

        if ($object->has_external_subscription) {
            my $ext_id = $object->external_subscription->external_identifier;
            $object->external_subscription->external_identifier($ext_id);
        }
    }

    return $object;
}

around 'remote_import' => sub {
    my $method      = shift;
    my $self        = shift;
    my ($params)    = @_;

    my $type        = (blessed($params) ? $params->subject_type : $params->{subject_type});

    return $self->$method(@_) if !$self->_is_remote_nps({ subject_type => $type });

    my $interface = $self->_assert_stuf_interface;

    ### Create this entry into our system.
    my $object = (blessed($params) ? $params : $self->object_from_params($params));

    my $config_interface = $self->schema->resultset('Interface')->find($self->config_interface_id);

    # If the object has a subscription but there is no uuid defined,
    # assume our current interface, most likely spoof mode
    if ($object->external_subscription && !$object->external_subscription->config_interface_uuid) {
        $object->external_subscription->config_interface_uuid(
            $config_interface->uuid
        );
    }

    $object = $self->_merge_existing_into_new($object);

    #### Now import it into StUF
    try {
        $self->schema->txn_do(
            sub {


                my $subscription = $object->has_external_subscription ? $object->external_subscription : undef;

                $self->_assert_config_interface_on_subscription($object);

                my $subject_params = {
                    personal_number   => $object->subject->personal_number,
                    personal_number_a => $object->subject->personal_number_a,
                };

                my $trigger_params = {
                    subject             => $subject_params,
                    config_interface_id => $self->config_interface_id,
                };

                if ($object->has_external_subscription) {
                    $trigger_params->{external_identifier} = $object->external_subscription->external_identifier;

                }
                else {
                    $object->external_subscription(
                        my $os
                            = Zaaksysteem::Object::Types::ExternalSubscription
                            ->new(
                            external_indentifier  => 'IN_PROGRESS',
                            config_interface_uuid => $config_interface->uuid,
                            interface_uuid        => $interface->uuid,
                            )
                    );
                }

                $object = $self->save($object);

                $trigger_params->{id} = $object->id;

                my $transaction = $interface->process_trigger('import_nps', $trigger_params);

                my $result = $transaction->get_processor_params->{result};

                if (!$result) {
                    throw(
                        'br/subject/remote_import/remote_error',
                        'Problem importing person because other party returned an error, transaction id: '
                        . ($transaction ? $transaction->id : '<unknown>')
                    );
                }
            }
        );
    } catch {
        throw(
            'br/subject/remote_import/remote_error',
            "$_"
        );
    };

    return $object->discard_changes(schema => $self->schema);
};

=head1 PRIVATE METHODS

=head2 _is_remote_nps

    $self->_is_remote_nps({ subject_type => 'person'});

    ### Returns 1 if $self->remote_search eq 'stuf';

Returns true when searching is remotely and given subject_type equals person.

=cut

sub _is_remote_nps {
    my $self        = shift;
    my $params      = shift;

    return 1 if (
        $params->{subject_type} &&
        $params->{subject_type} eq 'person' &&
        $self->remote_search &&
        lc($self->remote_search) eq 'stuf'
    );

    return;
}

=head2 _assert_stuf_interface

    my $interface = $self->_assert_stuf_interface;

Returns the primary STUF interface from L<Zaaksysteem::Backend::Sysin::Modules>

=cut

sub _assert_stuf_interface {
    my $self   = shift;
    my $params = shift;

    ### StUF Configuration
    my $stuf_cfg_iface = $self->schema->resultset('Interface')->search_active(
        {
            id      => $self->config_interface_id,
            module  => 'stufconfig',
        },
    )->first;

    throw(
        'br/subject/search_remote/no_stuf_cfg_iface',
        'Remote search requested, but no active StUF configuration found'
    ) unless $stuf_cfg_iface;

    my $interface = $stuf_cfg_iface->module_object->get_natuurlijkpersoon_interface($stuf_cfg_iface);

    ### Search object
    if (!$interface) {
        throw(
            'br/subject/search_remote/no_stuf_iface',
            'Remote search requested, but no active StUF NP Interface found'
        );
    }

    return $interface;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

