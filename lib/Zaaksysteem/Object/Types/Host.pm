package Zaaksysteem::Object::Types::Host;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw(Zaaksysteem::Object::Roles::Relation Zaaksysteem::Object::Roles::Security);

use Zaaksysteem::Types qw(ZSNetworkACLs JSONBoolean Timestamp FQDN Otap Betrokkene IPv4 IPv6 NonEmptyStr UUID);
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Types::Host - Built-in object type implementing
a class for customer.d configuration entries.

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 owner

The owner of the object

=cut

has owner => (
    is       => 'rw',
    isa      => Betrokkene,
    traits   => [qw[OA]],
    label    => 'The owner of the object type',
    required => 1,
);

=head2 fqdn

Fully qualified domainname

=cut

has fqdn => (
    is       => 'rw',
    isa      => FQDN,
    traits   => [qw[OA]],
    label    => 'Fully Qualified Domain Name',
    unique   => 1,
    required => 1,
);

=head2 ip

IPv4 IP address

=cut

has ip => (
    is       => 'rw',
    isa      => IPv4,
    traits   => [qw[OA]],
    label    => 'IP address of the host',
    unique   => 1,
    clearer  => '_clear_ip'
);


=head2 label

Human-readxiaible name that identifies the host entry

=cut

has label => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw[OA]],
    label    => 'Label',
    required => 1,
);

=head2 disabled

Boolean to indicate that this customer config is disabled, defaults to 'true'

=cut

has disabled => (
    is      => 'rw',
    isa     => JSONBoolean,
    traits  => [qw[OA]],
    label   => 'Disabled',
    default => 1,
    coerce  => 1,
);

=head2 ssl_key

Str value for the private key

=cut

has ssl_key => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => 'The private SSL key',
);

=head2 ssl_cert

Str value for the SSL certificate

=cut

has ssl_cert => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw[OA]],
    label  => 'The private SSL key',
);

=head2 new_empty

This instantiator returns a new instance of a generic type, with enough
default variables to make it validate. It can be used to save a temporary
ObjectType that will be edited later. It is also used by the form API for
creating empty forms.

=cut

sub new_empty {
    my $self = shift;

    $self->new(
        fqdn  => 'invalid.zaaksysteem.nl',
        label => 'Invalid host',
        owner => 'betrokkene-bedrijf-1',
        ip    => '127.0.0.1',
    );

}

=head2 relatable_types

Returns a list of types that can be related to a Host object

=cut

sub relatable_types {
    return qw (controlpanel instance);
}

=head2 set_instance

Arguments: $OBJECTMODEL, $INSTANCE_UUID

    $object->set_instance($c->model('Object'), '23232-aas-232-aa3-232323');

Removes previous instances, and sets the new instance on this object

=cut

sub set_instance {
    my $self            = shift;
    my $objectmodel     = shift;
    my $instance_uuid   = shift;

    ### remove relations of type
    for my $rel ($self->all_relations()) {
        my $related_object;
        if (blessed $rel->related_object) {
            $related_object = $rel->related_object;
        } else {
            $related_object = $objectmodel->retrieve(uuid => $rel->related_object_id);
        }

        if ($related_object->type eq 'instance') {
            $self->unrelate($related_object);
        }
    }

    if ($instance_uuid) {
        my $instance    = $objectmodel->retrieve(uuid => $instance_uuid);
        $self->relate($instance);
    }
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
