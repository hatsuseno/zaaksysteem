package Zaaksysteem::Timer;
use Moose;
use namespace::autoclean;

use Time::HiRes qw(gettimeofday tv_interval);

=head1 NAME

Zaaksysteem::Timer - Time things for the better

=head1 SYNOPIS

    use Zaaksysteem::Timer;

    my $timer = Zaaksysteem::Timer->new();
    $timer->start;
    $timer->time;
    $timer->stop;

=cut

has _starttime => (
    is      => 'rw',
    isa     => 'Any',
    lazy    => 1,
    default => sub {
        my $t0 = [gettimeofday];
        return $t0;
    }
);

=head1 METHODS

=head2 start

Set the start time for the timer

=cut

sub start {
    my $self = shift;
    return $self->_starttime(shift // [gettimeofday]);
}

=head2 time

Time (in ms) the duration between the start time and the now

=cut

sub time {
    my $self = shift;
    return tv_interval(shift // $self->_starttime) * 1000;
}

=head2 stop

Similar to C<time> but also resets the start time to C<now>

=cut

sub stop {
    my $self = shift;
    my $time = shift;
    my $elapsed = $self->time($time);
    $self->start unless $time;
    return $elapsed;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
