=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces::Casetype - Casetype API reference
documentation

=head1 NAMESPACE URL

    /api/v1/casetype

=head1 DESCRIPTION

This page documents the endpoints within the C<casetype> API namespace.

=head1 ENDPOINTS

=head2 Core endpoints

=head3 C<GET />

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<casetype|Zaaksysteem::Manual::API::V1::Types::Casetype> instances.

=head3 C<GET /[UUID]>

Returns a L<casetype|Zaaksysteem::Manual::API::V1::Types::Casetype> instance.

=head3 C<GET /list_allowed_users>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<casetype_acl|Zaaksysteem::Manual::API::V1::Types::CasetypeACL> instances.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
