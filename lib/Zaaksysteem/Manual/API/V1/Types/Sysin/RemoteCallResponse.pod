=head1 NAME

Zaaksysteem::Manual::API::V1::Types::RemoteCallResponse - An unformatted RPC reponse

=head1 Description

This API-document describes the usage of the
L<Zaaksysteem::Object::Types::RemoteCallResponse> object

The RemoteCallResponse is a simple container for RPC call responses.

=begin javascript

{
    "instance": {
        "call": "some_rpc_call",
        "data": { "response_data": "goes here" }
    },
    "reference": null,
    "type": "sysin/remote_call_response"
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 call E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Name of the RPC call that was executed.

=head2 data E<raquo> C<anything>

Data returned by the RPC call.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
