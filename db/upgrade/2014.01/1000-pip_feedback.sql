BEGIN;

CREATE TABLE message (
	id SERIAL PRIMARY KEY,
	message TEXT NOT NULL,
	subject_id VARCHAR,
	logging_id INTEGER NOT NULL REFERENCES logging(id),
	is_read BOOLEAN DEFAULT 'f'
);

COMMIT;