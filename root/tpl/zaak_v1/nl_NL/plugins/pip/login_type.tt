[% layout_type = 'simple' %]

<div class="pip-login">

    [% IF saml_error %]
        [% PROCESS saml_error_message_block %]
    [% END %]

    [%- intro_text = c.model('DB::Config').get_value('pip_login_intro') | trim;
    IF intro_text.length > 0 %]
    <div class="pip-overview-introduction">[% intro_text %]</div>
    [%- END %]

    <div class="pip-login-inner clearfix">
        [% IF citizen_idps.size >= 1 %]
        <div class="pip-login-block pip-login-block-left">
            <h2 class="pip-login-block-header">Inloggen als persoon</h2>

            <div class="pip-login-block-content">
                <p class="pip-login-block-content-intro">Bent u een (natuurlijk) persoon? Log dan in met uw DigiD inlogcode om gebruik te maken van "Mijn [% c.get_customer_info.naam %]"</p>

                [% FOREACH idp IN citizen_idps %]
                <a href="[% c.uri_for('/pip/login/natuurlijk_persoon', { idp_id => idp.id }) %]" class="button button-xlarge button-primary button-id">
                    Inloggen met [% idp.name %]
                </a>
                [% END %]

                <div class="prelogin-info prelogin-info-pip clearfix">
                    <div class="prelogin-logos col">
                        <a href="http://www.digid.nl/" class="prelogin-info-logo">
                            <img width="50" height="50"
                            src="images/digid_eo_rgb_50.gif" alt="digid
                            logo"/>
                        </a>
                    </div>

                    <div class="prelogin-info-tekst col">
                        <h3>Wat is DigiD?</h3>
                        <p>Met uw persoonlijke DigiD kunt u inloggen op
                        de website van de gemeente [% c.get_customer_info.naam %] om zaken te
                        regelen.
                        Bijvoorbeeld een vergunning of uittreksel. Uw
                        DigiD bestaat uit een gebruikersnaam en een
                        wachtwoord. Klik op het DigiD-logo voor meer
                        informatie of ga naar <a href="http://www.digid.nl">http://www.digid.nl</a></p>
                    </div>
                </div>
            </div>
        </div>
        [% END %]
    
        <div class="pip-login-block">
            <h2 class="pip-login-block-header">Inloggen als organisatie</h2>
            
            <div class="pip-login-block-content">
                <p class="pip-login-block-content-intro">
                    Bent u een bedrijf, stichting of vereniging? Klik dan hier om in te loggen op "Mijn [% c.get_customer_info.naam %]".
                </p>

                <a href="[% c.uri_for('/pip/login/bedrijf') %]" class="button-primary button-xlarge button button-id">
                    Inloggen als organisatie
                </a>
            </div> <!-- pip-block-content -->
        </div> <!-- pip-block -->
    </div> 
</div>

[% BLOCK saml_error_message_block %]
    <div class="saml_error">

    [% SWITCH saml_error %]
        [% CASE 'cancelled' %]
        <span>U heeft het inloggen bij DigiD geannuleerd.</span>
        [% CASE %]
        <span>
            Er is een fout opgetreden in de communicatie met
            <a href="https://www.digid.nl/">DigiD</a>. Probeert u het later
            nogmaals. Indien deze fout blijft aanhouden, kijk dan op de 
            website van <a href="https://www.digid.nl/">DigiD</a> voor de
            laatste informatie.
        </span>
    [% END %]

    </div>
[% END %]
