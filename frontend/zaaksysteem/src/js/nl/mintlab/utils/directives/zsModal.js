/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.directives')
		.directive('zsModal', [ '$interpolate', '$document', function ( $interpolate, $document ) {
			
			var addEventListener = window.zsFetch('nl.mintlab.utils.events.addEventListener'),
				removeEventListener = window.zsFetch('nl.mintlab.utils.events.removeEventListener'),
				doc = $document[0],
				tplEl = angular.element(
				'<div class="modal">' +
					'<div class="modal-backdrop">' +
					'</div>' +
					'<div class="modal-body">' +
						'<div class="modal-header clearfix">' +
							'<h2 class="modal-title">' + $interpolate.startSymbol() + 'title' + $interpolate.endSymbol() + '</h2>' +
							'<button class="modal-close" data-ng-click="closePopup()"><i class="mdi mdi-close"></i></button>' +
						'</div>' +
						'<div class="modal-content" ng-transclude>' +
						'</div>' +
					'</div>' +
				'</div>');
			
			return {
				scope: true,
				restrict: 'A',
				template: tplEl[0].outerHTML,
				transclude: true,
				link: function ( scope, element, attrs ) {

					var closeUnbind,
						destroyUnbind;
					
					function onKeyUp ( event ) {
						if(event.keyCode === 27) {
							clean();
							scope.closePopup();
						}
					}
					
					function clean ( ) {
						removeEventListener(doc, 'keyup', onKeyUp);
						if(closeUnbind) {
							closeUnbind();
						}
						if(destroyUnbind) {
							destroyUnbind();
						}
					}

					function setTitle ( ) {
						scope.title = attrs.zsModalTitle || scope.title;
					}
					
					addEventListener(doc, 'keyup', onKeyUp);
					
					closeUnbind = scope.$on('popupclose', clean);
					
					destroyUnbind = scope.$on('destroy', function ( ) {
						clean();
						scope.closePopup();
					});

					if (attrs.zsModalTitle === undefined) {
						setTitle();
					} else {
						attrs.$observe('zsModalTitle', setTitle);	
					}
					
				}

			};
			
		}]);
	
})();
