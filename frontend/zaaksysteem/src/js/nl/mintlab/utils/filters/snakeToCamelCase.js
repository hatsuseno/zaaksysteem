/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.filter('snakeToCamelCase', function ( ) {
			
			function convert ( string ) {
				return string[1].toUpperCase();
			}
			
			return function ( source ) {
				return source.replace(/(_([a-z]))/g, convert);
			};
		});
	
	
})();