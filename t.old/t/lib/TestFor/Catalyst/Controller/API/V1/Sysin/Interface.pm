package TestFor::Catalyst::Controller::API::V1::Sysin::Interface;
use base qw(ZSTest::Catalyst);

use Moose;
use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller:API::V1::Sysin::Interface - Proves the boundaries of our API: Interface

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Sysin/Interface.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/sysin/interface> namespace.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Create

=head3 trigger interface

    # curl -k -H https://localhost/api/v1/sysin/interface/304a7dc4-50ae-4ea5-a649-97882aeef7e0/trigger/custom_trigger_name

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-061178-b2be73",
   "result" : {
      "instance" : {
         "rows" : [
            {
               "instance" : {
                  "checksum" : "md5:b771ff6f06666e4eb88aab9f2131a68f",
                  "created" : "2015-09-01T12:54:11Z",
                  "id" : "8a9518d9-df04-41ce-8648-d0c3c72f84b5",
                  "last_modified" : "2015-09-01T12:54:14Z",
                  "metadata" : {},
                  "mimetype" : "text/plain",
                  "name" : "xentialfile.doc",
                  "size" : 2387,
                  "title" : "xentialfile"
               },
               "reference" : "8a9518d9-df04-41ce-8648-d0c3c72f84b5",
               "type" : "file"
            }
         ]
      },
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_sysin_interface_trigger : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech      = $zs->mech;
        my $iface     = $self->_create_xential_interface;
        my ($trans, $case, $case_document_id) = $self->_create_xential_transaction($iface);

        $mech->post_file(
            $mech->zs_url_base . '/api/v1/sysin/interface/' . $iface->uuid . '/trigger/api_post_file?transaction_uuid=' . $trans->uuid . '&case_uuid=' . $case->object_data->uuid,
            $zs->config->{filestore_test_file_path}
        );

        my $perl = $zs->get_json_as_perl($mech->content);

        is($perl->{result}->{type}, 'set', 'Got a set of results');
        is(@{ $perl->{result}->{instance}->{rows} }, 1, 'Got a single result');

        TODO : {
            local $TODO = ".docx assumption: get extension from Xential upload"; is($perl->{result}->{instance}->{rows}->[0]->{instance}->{name}, 'xentialfile.doc', 'Uploaded file with correct filename');
        }

    }, 'api/v1/sysin/interface/trigger: Trigger a transaction');
}

=head1 INTERNAL METHODS

=head2 _create_xential_interface

Creates a xential interface with create_interface_ok

=cut

sub _create_xential_interface {
    my $self    = shift;

    my $iface = $zs->create_xential_interface_ok();
    $iface->discard_changes;
    return $iface;
}

=head1 _create_xential_transaction

Creates a xential transaction, by creating a zaaktype, a subject, a case and the final transaction

=cut

sub _create_xential_transaction {
    my $self  = shift;
    my $iface = shift;

    my $casetype        = $zs->create_zaaktype_predefined_ok;
    my $casetypenode    = $casetype->zaaktype_node_id;

    ### First status
    my $status          = $casetypenode->zaaktype_statussen->search({ status => 1})->first;

    my $kenmerk         = $zs->create_zaaktype_kenmerk_ok(
        status              => $status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'xentialdocument',
            label           => 'Please attach a copy of your document',
            magic_string    => 'xential_document',
            value_type      => 'file',
        )
    );

    my $case    = $zs->create_case_ok(zaaktype => $casetype);
    my $subject = $zs->create_subject_ok;

    my $ta = $zs->create_transaction_ok({
        interface_id     => $iface->id,
        processor_params => {
            case             => $case->id,
            subject          => $subject->id,
            document_title   => 'xentialfile.doc',
            case_document_id => $kenmerk->id,
        }
    });

    $ta->discard_changes();
    $case->discard_changes();

    is($ta->processor_params->{document_title}, 'xentialfile.doc', 'Correctly filled xential transaction: filename');
    ok($ta->processor_params->{$_}, "Correctly filled xential transaction: $_") for qw/case subject case_document_id/;

    return ($ta, $case, $kenmerk->id);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
