package Zaaksysteem::Test::Version;

=head1 NAME

Zaaksysteem::Test::Version - Test version module of Zaaksysteem

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Version

=cut

use Zaaksysteem::Test;

sub test_version {

    use Zaaksysteem::Version qw(:version);

    is(major_version(), 3, "Currently at version 3");
    like(minor_version(), qr/^\d+$/, "Currently at sub version " . minor_version);
    like(patch_version(), qr/^\d+$/, "Currently at patch version " . patch_version);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
