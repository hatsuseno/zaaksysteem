package Zaaksysteem::Test::Backend::Filestore::ResultSet;
use strict;
use warnings;

=head1 NAME

Zaaksysteem::Test::Backend::Filestore::ResultSet - Test methods

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Backend::Filestore::ResultSet;

=head1 METHODS

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Test::Mocks;

use Zaaksysteem::Types qw/UUID/;


use Test::MockModule;
use Test::MockObject;
use Zaaksysteem::Backend::Filestore::ResultSet;

use BTTW::Tools;
use Path::Tiny;

my $TEMPFILE_OK    = _tempfile_ok();
my $TEMPFILE_EMPTY = _tempfile_empty();
my $TEMPFILE_VIRUS = _tempfile_virus(); # we don't care, see sub _tempfile_virus

my $TEMPFILE_SIZE  = 23;

my $MD5_HASH       = '6df23dc03f9b54cc38a0fc1483df6e21';
my $UUID_STRING    = '9fbf02dc-af5e-4e4e-9ca6-965dbb1134d7';

=head2 test_clamscan

clamscan will retun on normal behaviour, but throws various exceptions otherwise

=over

=item filestore/clamscan/file_not_found

missing filepath

=item filestore/clamscan/file_not_found: ...

file does not exist

=item filestore/clamscan/scanner_not_available

=item filestore/clamscan/virus_found

=back

Once this behaviour has been tested, we will continue with the next method,
but using a mocked verion of C<clamscan>

=cut

sub test_clamscan {
    
    # create a test object that has all internel $self-> methods mocked away
    my $test = Test::MockObject->new();
    $test->mock(
        'clamscan' => \&Zaaksysteem::Backend::Filestore::ResultSet::clamscan );
    
    my %mock;
    
    # create File::Scan::ClamAV mocked scanner objects that we can trace
    $mock{obj}{'File::Scan::ClamAV'}{ok}      = _mock_new__File_Scan_ClamAV__ok();
    $mock{obj}{'File::Scan::ClamAV'}{trouble} = _mock_new__File_Scan_ClamAV__trouble();
    $mock{obj}{'File::Scan::ClamAV'}{viruses} = _mock_new__File_Scan_ClamAV__virus();

    $mock{mod}{'File::Scan::ClamAV'}          = Test::MockModule->new('File::Scan::ClamAV');
    $mock{mod}{'Zaaksysteem::StatsD'}         = _mock_mod__Zaaksysteem_StatsD();

    # normal behaviour first, with operational scanner and clean file
    #
    $mock{mod}{'File::Scan::ClamAV'}->mock(
        'new' => sub { return $mock{obj}{'File::Scan::ClamAV'}{ok} }
    );
    
    lives_ok {
        $test->clamscan( "$TEMPFILE_OK" );
    } "method clamscan";

    throws_ok {
        $test->clamscan( )
    } qr|^filestore/clamscan/file_not_found|,
    "... and throws exception: missing filepath";
    # causes a 'Use of uninitialized value $path in ...' message twice, FIX that
    
    throws_ok {
        $test->clamscan( '/tmp/########' )
    } qr|^filestore/clamscan/file_not_found: (.*): /tmp/########|s,
    "... and throws exception: file does not exist";
    
    # expect exceptions when scanner is having troubles
    #
    $mock{mod}{'File::Scan::ClamAV'}->mock(
        'new' => sub { return $mock{obj}{'File::Scan::ClamAV'}{trouble} }
    );
    
    throws_ok {
        $test->clamscan( $TEMPFILE_OK )
    } qr|^filestore/clamscan/scanner_not_available|,
    "... and throws exception: scanner not available";

    # expect exceptions when scanner is reporting viruses
    #
    $mock{mod}{'File::Scan::ClamAV'}->mock(
        'new' => sub { return $mock{obj}{'File::Scan::ClamAV'}{viruses} }
    );
    throws_ok {
        $test->clamscan( $TEMPFILE_VIRUS )
    } qr|^filestore/clamscan/virus_found: .* 'trojan & worm' .*|,
    "... and throws exception: virus found";

}

=head2 test_filestore_create

A heavilly mocked test for this method ... maybe this indicates a challenge for
refactoring?

Mocked away are the following internal methods:

=over

=item ->create()

=item ->filestore_model()

=item ->result_source()

=item ->clamscan()

=back

Under normal circumstances, this should:

=over

=item C<create> a row in the database

We also do check we pass in all the right values that were passed into
C<filestore_create> or derived from the file

=item have a C<filestore_replicate>

=back

Besides normal behaviour, this will also test that we throw the exceptions we
know about:

=over

=item params/profile: ... missing: file_path

=item params/profile: ... missing: original_name

=item filestore/no_readable_file: File ...

=item filestore/create/empty: Will not add an empty file

=back

Whenever one off those exceptions occur, we should not see any calls to
C<create> that would add a row to the database.

We test for the propegated exceptions from C<clamscan>

=over

=item filestore/clamscan/virus_found: Virus 'scary-virus' ...

=item filestore/clamscan/scanner_not_available: ...

=back

Curently, no records should be created either

Furthermore, we check that we can pass in optional arguments

=over

=item Optional argument: id

=item Optional argument: force_mimetype

=item Optional argument: ignore_extension

This optional param has never been implemented

=back


=cut

sub test_filestore_create {
    
    # create a test object that has all internel $self-> methods mocked away
    #
    my $test = Test::MockObject->new();
    
    # ->filestore_create
    # the original codeblock
    $test->mock( 'filestore_create' =>
        \&Zaaksysteem::Backend::Filestore::ResultSet::filestore_create
    );
    
    # ->create()
    # it magically creates a new ResultSet object, we only need a few things
    $test->mock( 'create' =>
        \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__create
    );
    
    # ->filestore_model()
    # gives us an object that can tell us about get_default_engine
    $test->mock( 'filestore_model'=>
        \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__filestore_model
    );
    
    # ->result_source()
    # which returns an object that can do 'schema'
    # which has a resultset for 'Queue items'
    $test->mock( 'result_source' =>
        \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__result_source
    );
    
    # ->clamscan()
    # the actual scanner that lives-ok or throws errors when viruses are found
    $test->mock( 'clamscan' =>
        \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__clamscan_ok
    );
    
    my %mock;
    
    # modules can only be mocked when inside the sub test_
    $mock{mod}{'Zaaksysteem::StatsD'}
        = _mock_mod__Zaaksysteem_StatsD();
    $mock{mod}{'File::ArchivableFormats'}
        = _mock_mod__File_ArchivableFormats();
    $mock{mod}{'Digest::MD5::File'}
        = _mock_mod__Digest_MD5_File();
    $mock{mod}{'Data::UUID'}
        = _mock_mod__Data_UUID();
    $mock{sub}{'File::stat::size'}
        = _mock_sub__File_stat_size( $TEMPFILE_SIZE );
    
    # we should get a new filestore_row object back, but since this created
    # inside the filestore_create method and will call other methods like update
    # or delete on it, we will create it here and provide it from the
    # $self->create. So we can check what happened inside.
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}
        = _mock_obj__Zaaksysteem_Backend_Filestore_ResultSet__create();
    _mock_set__Zaaksysteem_Backend_Filestore_ResultSet__create(
        $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}
    );
    
    $mock{obj}{'DBIC::ResultSet'}{Queue}
        = _mock_obj__DBIC__resultset__Queue();
    _mock_set__DBIC_ResultSet( 'Queue' => $mock{obj}{'DBIC::ResultSet'}{Queue} );
    
    
    # test normal behaviour
    #
    note "normal behaviour";
    
    $test->clear();
    _mock_set__default_resultset_attributes( 'queue_items' => [] );
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    
    lives_ok {
        my $params = _filestore_create__default_params();
        my @result = $test->filestore_create( $params );
    }
    "filestore_create runs OK";
    
    $test->called_ok('create', "... and tried to create a database entry");
    
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}
        ->called_ok('discard_changes', "... and returned a newly fetched object");
    
    cmp_deeply (
        _mock_get__default_resultset_attributes()->{'queue_items'} =>
        bag ( _expected_filestore_replicate() ),
        "... and added to queue_items a 'filestore_replicate' item"
    );

    # Throws exception: missing: file_path
    #
    note "Throws exception: missing: file_path";
    
    $test->clear();
    _mock_set__default_resultset_attributes( 'queue_items' => [] );
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    
    throws_ok {
        my $params = _filestore_create__default_params();
        delete $params->{file_path};
        my @result = $test->filestore_create( $params );
    } qr|^params/profile: (.*)missing: file_path|s,
    "Throws exception: missing: file_path";
    
    ok ! $test->called('create'),
        "... and did not try to create a database entry";
    
    ok ! $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('discard_changes'),
        "... and thus does not discard_changes";
    
    cmp_deeply (
        _mock_get__default_resultset_attributes()->{'queue_items'} =>
        bag ( ),
        "... and did not add anything to queue_items at all"
    );

    # Throws exception: missing: original_name
    #
    note "Throws exception: missing: original_name";
    
    $test->clear();
    _mock_set__default_resultset_attributes( 'queue_items' => [] );
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    
    throws_ok {
        my $params = _filestore_create__default_params();
        delete $params->{original_name};
        my @result = $test->filestore_create( $params );
    } qr|^params/profile: (.*)missing: original_name|s,
    "Throws exception: missing: original_name";
    
    ok ! $test->called('create'),
        "... and did not try to create a database entry";
    
    ok ! $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('discard_changes'),
        "... and thus does not discard_changes";
    
    cmp_deeply (
        _mock_get__default_resultset_attributes()->{'queue_items'} =>
        bag ( ),
        "... and did not add anything to queue_items at all"
    );

    # Throws exception: no readable file
    #
    note "Throws exception: no readable file";
    
    $test->clear();
    _mock_set__default_resultset_attributes( 'queue_items' => [] );
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    
    throws_ok {
        my $params = _filestore_create__default_params();
        $params->{file_path} = 'tmp/########';
        my @result = $test->filestore_create( $params );
    } qr|^filestore/no_readable_file: File 'tmp/########' .*|s,
    "Throws exception: no readable file";
    
    ok ! $test->called('create'),
        "... and did not try to create a database entry";
    
    ok ! $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('discard_changes'),
        "... and thus does not discard_changes";
    
    cmp_deeply (
        _mock_get__default_resultset_attributes()->{'queue_items'} =>
        bag ( ),
        "... and did not add anything to queue_items at all"
    );

    # Throws exception: Will not add empty file
    #
    note "Throws exception: Will not add empty file";
    
    $test->clear();
    _mock_set__default_resultset_attributes( 'queue_items' => [] );
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    
    throws_ok {
        my $params = _filestore_create__default_params();
        $params->{file_path} = "$TEMPFILE_EMPTY";
        local $mock{sub}{'File::stat::size'} = _mock_sub__File_stat_size( 0 );
        my @result = $test->filestore_create( $params );
    } qr|^filestore/create/empty: Will not add an empty file|s,
    "Throws exception: Will not add empty file";
    
    ok ! $test->called('create'),
        "... and did not try to create a database entry";
    
    ok ! $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('discard_changes'),
        "... and thus does not discard_changes";
    
    cmp_deeply (
        _mock_get__default_resultset_attributes()->{'queue_items'} =>
        bag ( ),
        "... and did not add anything to queue_items at all"
    );

    # Throws exception: Virus Found
    #
    note "Throws exception: Virus Found";
    $test->clear();
    _mock_set__default_resultset_attributes( 'queue_items' => [] );
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    $test->mock( 'clamscan' => \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__clamscan_virus );
    
    throws_ok {
        my $params = _filestore_create__default_params();
        $params->{file_path} = "$TEMPFILE_VIRUS";
        my @result = $test->filestore_create( $params );
    } qr|^filestore/clamscan/virus_found: Virus 'scary-virus' .*|s,
    "Throws exception: Virus Found";
    
    ok ! $test->called('create'),
        "... and did not try to create a database entry";
    
    ok ! $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('discard_changes'),
        "... and thus does not discard_changes";
    
    cmp_deeply (
        _mock_get__default_resultset_attributes()->{'queue_items'} =>
        bag ( ),
        "... and did not add anything to queue_items at all"
    );
    # restore
    $test->mock( 'clamscan' => \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__clamscan_ok );


    # Throws exception: Scanner Not Available
    #
    note "Throws exception: Scanner Not Available";
    
    $test->clear();
    _mock_set__default_resultset_attributes( 'queue_items' => [] );
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    $test->mock( 'clamscan' => \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__clamscan_trouble );
    
    throws_ok {
        my $params = _filestore_create__default_params();
        my @result = $test->filestore_create( $params );
    } qr|^filestore/clamscan/scanner_not_available: (.*)|s,
    "Throws exception: Scanner Not Available";

    ok ! $test->called('create'),
        "... and did not try to create a database entry";

    ok ! $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->called('discard_changes'),
        "... and thus does not discard_changes";

    cmp_deeply (
        _mock_get__default_resultset_attributes()->{'queue_items'} =>
        bag ( ),
        "... and did not add anything to queue_items at all"
    );

    # restore
    $test->mock( 'clamscan' => \&_mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__clamscan_ok );

    # Optional argument: id
    #
    note "Optional argument: id";
    
    $test->clear();
    _mock_set__default_resultset_attributes( 'queue_items' => [] );
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    
    lives_ok {
        my $params = _filestore_create__default_params();
        $params->{id} = "MY_OWN_ID_000";
        my @result = $test->filestore_create( $params );
    } "filestore_create runs OK with optional param 'id'";
    
    $test->called_ok('create', "... and tried to create a database entry");
    
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}
        ->called_ok('discard_changes', "... and returned a newly fetched object");
    
    cmp_deeply (
        _mock_get__default_resultset_attributes()->{'queue_items'} =>
        bag ( _expected_filestore_replicate() ),
        "... and added to queue_items a 'filestore_replicate' item"
    );
    
    # Optional argument: force_mimetype
    #
    note "Optional argument: force_mimetype";
    
    $test->clear();
    _mock_set__default_resultset_attributes( 'queue_items' => [] );
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    
    lives_ok {
        my $params = _filestore_create__default_params();
        $params->{force_mimetype} = "test/forced";
        my @result = $test->filestore_create( $params );
    } "filestore_create runs OK with optional param 'force_mimetype'";
    
    $test->called_ok('create', "... and tried to create a database entry");
    
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}
        ->called_ok('discard_changes', "... and returned a newly fetched object");
    
    cmp_deeply (
        _mock_get__default_resultset_attributes()->{'queue_items'} =>
        bag ( _expected_filestore_replicate() ),
        "... and added to queue_items a 'filestore_replicate' item"
    );
    
    # Optional argument: ignore_extension
    #
    # This optional param has never been implemented
    #
    note "Optional argument: ignore_extension";
    
    $test->clear();
    _mock_set__default_resultset_attributes( 'queue_items' => [] );
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    
    lives_ok {
        my $params = _filestore_create__default_params();
        $params->{ignore_extension} = 1;
        my @result = $test->filestore_create( $params );
    } "filestore_create runs OK with optional param 'ignore_extension => 1'";
    lives_ok {
        my $params = _filestore_create__default_params();
        $params->{ignore_extension} = 0;
        my @result = $test->filestore_create( $params );
    } "filestore_create runs OK with optional param 'ignore_extension => 0'";
    
    # Optional argument: non_existing_param
    #
    # This should just pass
    #
    note "Optional argument: non_existing_param";
    
    $test->clear();
    _mock_set__default_resultset_attributes( 'queue_items' => [] );
    $mock{obj}{'Zaaksysteem::Backend::Filestore::ResultSet'}{internal}->clear();
    
    lives_ok {
        my $params = _filestore_create__default_params();
        $params->{non_existing_param} = undef;
        my @result = $test->filestore_create( $params );
    } "filestore_create runs OK with optional param 'non_existing_param => undef'";
    #
    # By design, define_profile will not barf for params not specified in the
    # profile, please be no longer surprised that this is OK and thus lives_ok
    # passes.
    
}

################################################################################
#
# done testing
#
# and 500 lines of mocking follow

sub _filestore_create__default_params {
    return {
        file_path     => $TEMPFILE_OK->stringify,
        original_name => 'Temporary File',
    }
}

# _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__create
#
# check for passed params that will be inserted into the database
# and return a previously stored object
#
sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__create {
    my ($self, $params) = @_;
    cmp_deeply ( $params => subhashof( _expected_params() ),
        "got the right param/values to create new row"
    );

    return _mock_get__Zaaksysteem_Backend_Filestore_ResultSet__create();
}

# returns a mocked Zaaksysteem::Backend::Filestore::ResultSet object that
# implements methods being called lateron inside our filestore_create
# - delete
# - discard changes
# - id 
# - update
#
sub _mock_obj__Zaaksysteem_Backend_Filestore_ResultSet__create {
    my $mock = Test::MockObject->new();
    $mock->set_true('delete', 'update');
    $mock->set_always('id' => "MY_NEW_ID_123");
    $mock->mock('discard_changes' => sub { return $_[0] } ); # self

    return $mock
}

# sets and gets a mocked Zaaksysteem::Backend::Filestore::ResultSet object like
# the above
{
my $mock;
sub _mock_get__Zaaksysteem_Backend_Filestore_ResultSet__create { return $mock }
sub _mock_set__Zaaksysteem_Backend_Filestore_ResultSet__create { $mock = shift }
}

sub _expected_params {
    return {
        is_archivable     => any ( 9, undef ), # that is us and not original module, or a bad mime-type
        md5               => $MD5_HASH, # from mocked Digest::MD5::File
        mimetype          => any ( 'test/mocked', 'test/forced' ),
        original_name     => "Temporary File",
        size              => $TEMPFILE_SIZE,
        storage_location  => [ "Mocked FileStore Engine" ],
        uuid              => $UUID_STRING,
        virus_scan_status => any ( 'pending', 'ok', 'found' ),
        id                => "MY_OWN_ID_000", # only for passed in `id` by caller
    }
}

#
# mock method Zaaksysteem::Backend::Filestore::ResultSet->filestore_model()
#

sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__filestore_model {
    my $mock = Test::MockObject->new();
    $mock->mock('get_default_engine' => \&_mock_obj__Zaaksysteem_Filestore_Model__get_default_engine );
    return $mock;
}

sub _mock_obj__Zaaksysteem_Filestore_Model__get_default_engine {
    my $mock = Test::MockObject->new();
    $mock->mock( 'write' => \&_mock_sub__Zaaksysteem_Filestore_Engine__write );
    $mock->set_always( 'name' => "Mocked FileStore Engine" );

    return $mock;
}

# _mock_sub__Zaaksysteem_Filestore_Engine__write
#
# a mocked method for, being called on the default retirned FileStore::Engine
#
sub _mock_sub__Zaaksysteem_Filestore_Engine__write {
    my $self = shift;
    my @args = @_;
    is_uuid_string ( $args[0], "Storage Engine - write method argument 0 is a UUID conferment string" );
    is_filehandle  ( $args[1], "Storage Engine - write method argument 1 might be a FileHandle" ); # fake too
    return 1; # or whatever
}

#
# mock internal sub Zaaksysteem::Backend::Filestore::ResultSet clamscan
# we've tested the bahaviour at the top of this test
#

# _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__clamscan_ok
#
# mocks internal method clamscan()
# - exit normally
#
sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__clamscan_ok {
    my $self = shift; # not interested in invocant
    my @args = @_;
    my $file = $TEMPFILE_OK;

    # ensure that we internally did pass in a filepath
    cmp_deeply (
        \@args => [
            "$file"
        ],
        "clamscan called correctly for OK"
    );

    # NOTE: the original returns implicitly the value of the last statement
    return !! undef() # which is the result of the comparison ($stat eq "FOUND")
}

# _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__clamscan_virus
#
# mocks internal method clamscan()
# -throws exception 'filestore/clamscan/virus_found' 
#
sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__clamscan_virus {
    my $self = shift; # not interested in invocant
    my @args = @_;
    my $file = $TEMPFILE_VIRUS;
    my $virus = 'scary-virus';

    # ensure that we internally did pass in a filepath
    cmp_deeply (
        \@args => [
            "$file"
        ],
        "clamscan called correctly for VIRUS"
    );
    
    throw(
        "filestore/clamscan/virus_found",
        "Virus '$virus' aangetroffen in bestand, niet toegevoegd."
    );
};

# mock internal method clamscan()
#
# throws exception 'filestore/clamscan/scanner_not_available' 
#
sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__clamscan_trouble {
    my $self = shift; # not interested in invocant
    my @args = @_;
    my $file = $TEMPFILE_OK;
    
    # ensure that we internally did pass in a filepath
    cmp_deeply (
        \@args => [
            "$file"
        ],
        "clamscan called correctly for TROUBLE"
    );
    
    throw(
        "filestore/clamscan/scanner_not_available",
        "Virusscanner niet operationeel",
        { error => undef }
    )
};

# _mock_new__File_Scan_ClamAV
#
# Creates a File::Scan::ClamAV mocked scanner object to be returned when mocking
# the File::Scan::ClamAV::new() method.
#
# The returned mocked object only implements scanner methods that are being used
# internally by our clamscan method:
# - errstr
# - ping
# - streamscan
#
# accepts a list of virusses, empty for non, undef for broken ClamAV
#
sub _mock_new__File_Scan_ClamAV {
    my $args = shift;
    my $mock = Test::MockObject->new();
    $mock->set_isa('File::Scan::ClamAV');

    $mock->mock(
        errstr => sub {
            return defined $args ? "" : "Unknown reponse from ClamAV service ..."
        }
    );

    $mock->mock(
        ping => sub {
            return defined $args
        }
    );

    $mock->mock(
        streamscan => sub {
            my $data = shift; 
            my $status = scalar @$args ? "FOUND" : "OK";
            my $virus = join ' & ', @$args;
            return ($status, $virus)
        }
    );

    return $mock;
}

sub _mock_new__File_Scan_ClamAV__ok {
    return _mock_new__File_Scan_ClamAV( [ ] );
}

sub _mock_new__File_Scan_ClamAV__trouble {
    return _mock_new__File_Scan_ClamAV( undef );
}

sub _mock_new__File_Scan_ClamAV__virus {
    return _mock_new__File_Scan_ClamAV( [ 'trojan', 'worm' ] );
}

#
# mock Queue
#

sub _mock_sub__Zaaksysteem_Backend_Filestore_ResultSet__result_source{
    my $mock = Test::MockObject->new();
    $mock->mock(
        schema => \&_mock_obj__DBIC_schema
    );

    return $mock
}

sub _mock_obj__DBIC_ResultSource {
    my $mock = Test::MockObject->new();
    $mock->mock(
        schema => \&_mock_sub__DBIC_schema
    );
    
    returnm $mock
}

sub _mock_obj__DBIC_schema {
    my $mock = Test::MockObject->new();
    $mock->mock(
        resultset => sub {
            my $self = shift;
            my $resultset_name = shift;
            my $resultset = _mock_get__DBIC_ResultSet($resultset_name)
                or die "missing mocked resultset $resultset_name\n";
            return $resultset
        }
    );
    $mock->mock(
        default_resultset_attributes => sub {
            my $self = shift; # don't want invocant
            return _mock_get__default_resultset_attributes( )
        }
    );
    
    return $mock
}

sub _mock_obj__DBIC__resultset__Queue {
    my $mock = Test::MockObject->new();
    $mock->mock(
        create_item => sub {
            my $self = shift;
            my @args = @_;
            cmp_deeply (
                \@args => [
                    ignore(), # type
                    {
                        label => ignore(),
                        data  => ignore(), # hashref of whatever
                    }
                ],
                "... 'Queue' got right data for creating row"
            );
            my $qitem = {
                type  => $args[0],
                label => $args[1]->{label},
                data  => $args[1]->{data}
            };
            return $qitem
        }
    );
    
    return $mock
}




{ my %data;
sub _mock_set__DBIC_ResultSet { my $name = shift; $data{ $name } = shift }
sub _mock_get__DBIC_ResultSet { my $name = shift; return $data{ $name } }
}

{ my $data = {}; 
sub _mock_set__default_resultset_attributes { my $name = shift; $data->{$name} = shift }
sub _mock_get__default_resultset_attributes { return $data }
}

#
# mock module Zaaksysteem::StatsD
#

sub _mock_mod__Zaaksysteem_StatsD {
    my $mock = Test::MockModule->new("Zaaksysteem::StatsD");
    $mock->mock(
        'statsd' => sub {
            my $mock_obj = Test::MockObject->new();
            $mock_obj->set_true('start', 'end', 'increment');
            return $mock_obj;
        }
    );
    return $mock;
}

#
# mock module File::ArchivableFormats
#

sub _mock_mod__File_ArchivableFormats {
    my $mock_mod = Test::MockModule->new('File::ArchivableFormats');
    $mock_mod->mock(
        'new' => sub {
            my $mock_obj = Test::MockObject->new();
            $mock_obj->mock(
                identify_from_mimetype => sub {
                    my ($self, $mime_type) = @_;
                    return _file_archivable_test_force() if $mime_type eq 'test/forced';
                    return { mime_type => 'test/does_not_exist' }
                }
            );
            $mock_obj->set_always(
                identify_from_path => _file_archivable_test_plain(),
            );
            return $mock_obj
        }
    );
    return $mock_mod
}

sub _file_archivable_test_plain {
    return {
        DANS => {
            allowed_extensions => [
                ".asc",
                ".txt"
            ],
            archivable         => 9, # that is us and not original module
            types              => [
                "Plain text (Unicode)",
                "Plain text (Non-Unicode)",
                "Statistical data (data (.csv) + setup)",
                "Raspter GIS (ASCII GRID)",
                "Raspter GIS (ASCII GRID)"
            ]
        },
        mime_type => "test/mocked"
    }
}

sub _file_archivable_test_force {
    return {
        DANS => {
            allowed_extensions => [
                ".tst"
            ],
            archivable         => 9, # that is us and not original module
            types              => [
                "Forced Test File",
            ]
        },
        mime_type => "test/forced"
    }
}

# mock module Digest::MD5::File
#
# to give always the same MD5 string
#

sub _mock_mod__Digest_MD5_File {
    my $mock = Test::MockModule->new('Zaaksysteem::Backend::Filestore::ResultSet', no_auto => 1);
    # since it's imported into the module's namespace we need to mock ourself
    $mock->mock(
        'file_md5_hex' => sub {
            my $filepath = shift;
            like $filepath, qr|^/tmp/.{10}$|,
                "... called with a expected filepath";
            return $MD5_HASH;
            }
    );
    return $mock
}


sub _mock_mod__Data_UUID {
    my $mock_mod = Test::MockModule->new('Data::UUID');
    $mock_mod->mock(
        'new' => sub {
            my $mock_obj = Test::MockObject->new;
            $mock_obj->set_always( 'create_str' => $UUID_STRING );
            return $mock_obj;
        }
    );
    return $mock_mod;
}


# set the file size for File::stat::size to a fixed answer
sub _mock_sub__File_stat_size {
    my $size = shift || 0;
    my $mock = Sub::Override->new();
    $mock->replace(
        'File::stat::size' => sub { $size }
    );
    return $mock;
}

sub _expected_filestore_replicate {
    return +{
        type  => 'filestore_replicate',
        label => ignore(), # Dutch labels may get translated
        data  => {
            filestore_id => 'MY_NEW_ID_123'
        }
    }
}

#
# utility
#

sub is_uuid_string {
    my ( $value, $message ) = @_;
    ok ( UUID->check($value), $message )
}

sub is_filehandle {
    my ( $test_value, $message ) = @_;
    ok ( defined $test_value, $message ); # nope this doesn't test that it actually is a FH
}

sub _tempfile_ok {
    my $file = Path::Tiny->tempfile();
    $file->spew( 'this is a test document' ); # 4dbee61dcd3395d95628595f89d1109c
    return $file
}

sub _tempfile_empty {
    my $file = Path::Tiny->tempfile();
    return $file
}

sub _tempfile_virus {
    my $file = Path::Tiny->tempfile();
    $file->spew( '** I AM INMFECTED **' ); # or what ever, we do not call ClamAV
    return $file
}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

1;