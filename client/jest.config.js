module.exports = {
  // This should correspond with the property keys used in the
  // `DefinePlugin` configuration object in `webpack/config/app/plugins.js`:
  globals: {
    ENV: false,
  },
  moduleNameMapper: {
    '/propCheck$': '<rootDir>/__mocks__/propCheck.js',
    // Specific webpack raw loader stub for scss (reads from the file system):
    '/styles/_breakpoints\\.scss$': '<rootDir>/__mocks__/breakpoints.js',
    // Generic return values for otherwise ignored
    // imports that would throw a syntax error:
    '\\.(html)$': '<rootDir>/__mocks__/htmlMock.js',
    '\\.(css|scss)$': '<rootDir>/__mocks__/styleMock.js',
  },
  // setup for fakes/mocks/stubs and such
  setupTestFrameworkScriptFile: '<rootDir>/test/jest-setup.js',
  testMatch: [
    // Old karma tests:
    '<rootDir>/src/**/test.js',
    // New tests:
    '<rootDir>/src/**/*.test.js',
  ],
};
