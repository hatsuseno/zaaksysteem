let secondsDone = 0,
    timeStart,
    timeEnd,
    failedExpectation,
    specCount = 0,
    executableSpecCount = 0,
    pendingSpecs = [],
    failedSpecs = [],
    failedSuites = [],
    failureCount = 0,
    ansi = {
        green: '\x1B[32m',
        red: '\x1B[31m',
        yellow: '\x1B[33m',
        none: '\x1B[0m'
    },
    colored = function(color, str) {
        return ansi[color] + str + ansi.none;
    },
    suiteFailureDetails = result => {
        let i;

        for (i = 0; i < result.failedExpectations.length; i++) {
            process.stdout.write(colored('red', `\nAn error was thrown in an afterAll\nAfterAll ${result.failedExpectations[i].message}\n`));
        }
    },
    myReporter = {
        jasmineStarted: suiteInfo => {
            process.stdout.write(`\nStarted on ${suiteInfo.totalSpecsDefined} specs\n`);
            timeStart = new Date();
        },
        jasmineDone: result => {
            let seconds;
            
            if (failedSpecs.length > 0) {
                process.stdout.write('\n\nFailures:');
            }
            for (let i = 0; i < failedSpecs.length; i++) {
                process.stdout.write(`\n${i + 1} - ${failedSpecs[i].fullName} (${failedSpecs[i].path})`);

                for (let j = 0; j < failedSpecs[i].failedExpectations.length; j++) {
                    failedExpectation = failedSpecs[i].failedExpectations[j];
                    process.stdout.write(colored('red', `\n    ${failedExpectation.message}`));
                }
            }

            // if (pendingSpecs.length > 0) {
            //     process.stdout.write('\n\nPending:');
            // }
            // for (let i = 0; i < pendingSpecs.length; i++) {
            //     let pendingReason = pendingSpecs[i].pendingReason && pendingSpecs[i].pendingReason !== '' ? pendingSpecs[i].pendingReason : 'No reason given';

            //     process.stdout.write(`\n${i + 1} - ${pendingSpecs[i].fullName}\n`);
            //     process.stdout.write(colored('yellow', `    ${pendingReason}`));
            // }

            timeEnd = new Date();
            seconds = (timeEnd - timeStart) / 1000;
            process.stdout.write(`\n\nRan ${executableSpecCount} of ${specCount} specs with ${failureCount} failures and ${pendingSpecs.length} pending. (${seconds} seconds)\n\n`);

            for (let i = 0; i < failedSuites.length; i++) {
                suiteFailureDetails(failedSuites[i]);
            }

            if (result && result.failedExpectations) {
                suiteFailureDetails(result);
            }

            if (result && result.order && result.order.random) {
                process.stdout.write(`Randomized with seed ${result.order.seed}\n`);
            }
        },
        specDone: result => {
            let seconds;

            timeEnd = new Date();
            seconds = (timeEnd - timeStart) / 1000;
            specCount++;

            if (seconds > secondsDone + 300) {
                secondsDone = seconds;
                process.stdout.write(` (${specCount} specs done at ${Math.floor(seconds / 60)} minutes)\n`);
            }

            if (result.status === 'pending') {
                pendingSpecs.push(result);
                executableSpecCount++;
                process.stdout.write(colored('yellow', '*'));
                return;
            }

            if (result.status === 'passed') {
                executableSpecCount++;
                process.stdout.write(colored('green', '.'));
                return;
            }

            if (result.status === 'failed') {
                let message = result.failedExpectations[0].stack,
                    pathwithoutPrefix = message.substring(message.indexOf('client/test/e2e/tests/') + 22),
                    path = pathwithoutPrefix.substring(0, pathwithoutPrefix.indexOf(':'));

                failureCount++;
                result.path = path;
                failedSpecs.push(result);

                executableSpecCount++;
                process.stdout.write(colored('red', `\n${specCount} - ${path} - ${result.failedExpectations[0].message}\n`));
                return;
            }
        },
        suiteDone: result => {
        if (result.failedExpectations && result.failedExpectations.length > 0) {
            failureCount++;
            failedSuites.push(result);
            }
        }
    };

module.exports = myReporter;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
