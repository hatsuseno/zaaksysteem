import {
    openView,
    loadMore,
    countCaselists,
    listCases
} from './../../functions/mor/mor';
import navigate from './../../functions/common/navigate';

describe('when opening the mor app in the open view and then opening the closed view', () => {

    beforeAll(() => {
        navigate.as('mor', '/mor/zaken/open');
        openView('afgehandeld');
    });

    it('the view should contain one caseList', () => {
        expect(countCaselists()).toEqual(1);
    });

    it('the caseList should contain the right cases', () => {
        const expected = [150, 149, 148, 147, 146, 145, 144, 143, 142, 141];

        expect(listCases(1)).toEqual(expected);
    });

    it('the view should contain an option to retrieve more cases', () => {
        expect(loadMore.isDisplayed()).toBe(true);
    });

});

describe('when opening the mor app in the closed view and then opening the open view', () => {

    beforeAll(() => {
        navigate.as('mor', '/mor/zaken/afgehandeld');
        openView('open');
    });

    it('the view should contain two caseLists', () => {
        expect(countCaselists()).toEqual(2);
    });

    it('the caseList should contain the right cases', () => {
        const expected = [136, 137, 138, 139];

        expect(listCases(1)).toEqual(expected);
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
