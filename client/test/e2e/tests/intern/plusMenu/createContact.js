import navigate from './../../../functions/common/navigate';
import {
    createContact
} from './../../../functions/intern/plusMenu';
import {
    search,
    countResults
} from './../../../functions/intern/universalSearch';

describe('when opening the form to register a contact', () => {

    beforeAll(() => {

        navigate.as();
        
    });

    describe('and registering a citizen with a residence address', () => {

        let newContact = {
            type: 'natuurlijk_persoon',
            lastName: 'citizenResidence'
        };
    
        beforeAll(() => {
    
            createContact(newContact);

            search(newContact.lastName);

        });
    
        it('there should be a snack with a link', () => {

            expect(countResults()).toEqual(1);
        
        });

    });

    describe('and registering a citizen with a correspondence address', () => {

        let newContact = {
            type: 'natuurlijk_persoon',
            lastName: 'citizenCorrespondence',
            correspondenceAddress: 'yes'
        };
    
        beforeAll(() => {
    
            createContact(newContact);

            search(newContact.lastName);

        });
    
        it('there should be a snack with a link', () => {
    
            expect(countResults()).toEqual(1);
    
        });
    
    });

    describe('and registering a citizen with a foreign address', () => {

        let newContact = {
            type: 'natuurlijk_persoon',
            lastName: 'citizenForeign',
            country: 'Marokko'
        };
    
        beforeAll(() => {
    
            createContact(newContact);

            search(newContact.lastName);

        });
    
        it('there should be a snack with a link', () => {
    
            expect(countResults()).toEqual(1);
    
        });
    
    });

    describe('and registering an organisation with an establishment address', () => {

        let newContact = {
            type: 'bedrijf',
            tradeName: 'companyEstablishment'
        };
    
        beforeAll(() => {
    
            createContact(newContact);

            search(newContact.tradeName);

        });
    
        it('there should be a snack with a link', () => {
    
            expect(countResults()).toEqual(1);
    
        });
    
    });

    describe('and registering an organisation with a foreign address', () => {

        let newContact = {
            type: 'bedrijf',
            tradeName: 'companyForeign',
            country: 'Marokko'
        };
    
        beforeAll(() => {
    
            createContact(newContact);

            search(newContact.tradeName);

        });
    
        it('there should be a snack with a link', () => {
    
            expect(countResults()).toEqual(1);
    
        });
    
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
