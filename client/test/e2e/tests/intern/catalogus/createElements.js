import navigate from './../../../functions/common/navigate';
import {
    createFolder,
    createCaseAttribute,
    createEmailTemplate,
    createDocumentTemplate,
    closeDialog,
    startCreateCasetype,
    startCreateObjecttype,
    startEditItem,
    closeModal
} from './../../../functions/intern/catalogus';
import {
    enterCasetypeName,
    enterIdentification,
    enterPrincipleNational,
    enterTermNational,
    enterTermService,
    publishCasetype
} from './../../../functions/intern/casetypeManagement';
import {
    enterObjecttypeName,
    enterTitle,
    enterCategory,
    addCaseAttribute,
    setCaseAttribute,
    publishObjecttype
} from './../../../functions/intern/objecttypeManagement';

describe('when opening the catalogus and creating a folder', () => {

    beforeAll(() => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        createFolder('Test folder');

    });

    it('the folder should have been created', () => {

        expect($('.breadcrumb-item-label').getText()).toEqual('Test folder');

    });

});

describe('when opening the catalogus and creating an attribute of type text', () => {

    beforeAll(() => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        const data = {
            name: 'Test tekstveld',
            type: 'Tekstveld'
        };

        createCaseAttribute(data);

    });

    it('it should have been created', () => {

        expect($('.table-closed').getText()).toContain('Test tekstveld');

    });

});

describe('when opening the catalogus and creating an attribute of type option', () => {

    beforeAll(() => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        const data = {
            name: 'Test enkelvoudige keuze',
            type: 'Enkelvoudige keuze',
            options: ['Optie 1', 'Optie 2', 'Optie 3']
        };

        createCaseAttribute(data);

    });

    it('it should have been created', () => {

        expect($('.table-closed').getText()).toContain('Test enkelvoudige keuze');

    });

    it('it should have the correct settings', () => {

        startEditItem('Test enkelvoudige keuze');

        expect($('.multiple-options').getText()).toContain('Optie 1');
        expect($('.multiple-options').getText()).toContain('Optie 2');
        expect($('.multiple-options').getText()).toContain('Optie 3');

    });

    afterAll(() => {

        closeDialog();

    });


});

describe('when opening the catalogus and creating an email template', () => {

    beforeAll(() => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        const data = {
            name: 'Test emailsjabloon',
            subject: 'Onderwerp',
            content: 'Bericht'
        };

        createEmailTemplate(data);

    });

    it('it should have been created', () => {

        expect($('.table-closed').getText()).toContain('Test emailsjabloon');

    });

    it('it should have the correct settings', () => {

        startEditItem('Test emailsjabloon', false);

        expect($('[data-ng-model="label"]').getAttribute('value')).toEqual('Test emailsjabloon');
        expect($('[data-ng-model="subject"]').getAttribute('value')).toEqual('Onderwerp');
        expect($('[data-ng-model="message"]').getAttribute('value')).toEqual('Bericht');

    });

    afterAll(() => {

        closeModal();

    });

});

describe('when opening the catalogus and creating a document template', () => {

    beforeAll(() => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        const data = {
            name: 'Test documentsjabloon',
            document: 'casenumber.odt'
        };

        createDocumentTemplate(data);

    });

    it('it should have been created', () => {

        expect($('.table-closed').getText()).toContain('Test documentsjabloon');

    });

});

describe('when opening the catalogus and creating a casetype', () => {

    beforeAll(() => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        startCreateCasetype();

        enterCasetypeName('Test zaaktype created');

        enterIdentification('1');

        enterPrincipleNational('1');

        enterTermNational('1');

        enterTermService('1');

        publishCasetype();

    });

    it('it should have been created', () => {

        expect($('.table-closed').getText()).toContain('Test zaaktype created');

    });

});

describe('when opening the catalogus and creating an objecttype', () => {

    beforeAll(() => {

        navigate.as('admin', '/beheer/bibliotheek/57');

        startCreateObjecttype();

        enterObjecttypeName('Test objecttype created');

        enterTitle('Test objecttype titel');

        enterCategory('- Create elements');

        addCaseAttribute('omschrijving');

        const data = {
            index: true,
            title: 'Omschrijving',
            required: true
        };

        setCaseAttribute(data);

        publishObjecttype(3);

        navigate.as('admin', '/beheer/bibliotheek/57');

    });

    it('it should have been created', () => {

        expect($('.table-closed').getText()).toContain('Test objecttype created');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
