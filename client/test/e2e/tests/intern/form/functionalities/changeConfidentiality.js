import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    changeConfidentiality
} from './../../../../functions/common/form';
import {
    getSummaryValue
} from './../../../../functions/intern/caseView/caseMenu';

const testData = [
    'Openbaar',
    // 'Intern', ZS-15730
    'Vertrouwelijk'
];

for ( const index in testData ) {

    describe(`when starting a registration form and setting confidentiality to ${testData[index]}`, () => {

        beforeAll(() => {

            navigate.as();

            const data = {
                casetype: 'Wijzig vertrouwelijkheid bij registratie aan',
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: 'behandelaar'
            };

            startForm(data);

            goNext();

            changeConfidentiality(testData[index]);

            goNext();

        });

        it(`the confidentiality should have been set to ${testData[index]}`, () => {

            expect(getSummaryValue('Vertrouwelijkheid')).toEqual(testData[index]);

        });

    });

}

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
