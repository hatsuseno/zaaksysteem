import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import caseAttribute from './../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const testData = [ 'behandelaar', 'balie', 'telefoon', 'post', 'email', 'webformulier', 'sociale media'];

for ( const index in testData ) {

    describe(`when opening a registration form with contactchannel ${testData[index]}`, () => {

        beforeAll(() => {

            navigate.as();

            const data = {
                casetype: 'Systeemvoorwaarde contactkanaal',
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: testData[index]
            };

            startForm(data);

            choice.$('[value="Ja"]').click();

        });

        for ( const attribute in testData ) {

            const dataName = testData[attribute].replace(' ', '_');
            const state = testData[index] === testData[attribute] ? 'True' : 'False';

            it(`the attribute for ${testData[attribute]} should be ${state}`, () => {

                expect(caseAttribute.getClosedValue($(`[data-name="contactkanaal_${dataName}"]`))).toEqual(state);

            });

        }

    });

}

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
