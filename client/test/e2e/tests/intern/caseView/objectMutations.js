import navigate from './../../../functions/common/navigate';
import {
    checkObjectButtonPresence,
    advance,
    createObjectMutationCreate,
    createObjectMutationMutate,
    createObjectMutationDelete,
    getAdvanceButtonText,
    editObjectMutation,
    startObjectMutation,
    performObjectMutationAction,
    deleteObjectMutation
} from './../../../functions/intern/caseView/casePhase';
import {
    openPhase
} from './../../../functions/intern/caseView/caseNav';
import waitForElement from './../../../functions/common/waitForElement';
import caseAttribute from './../../../functions/common/input/caseAttribute';
// import {
//     getTitle,
//     getValues
// } from './../../../functions/intern/objectView';
import {
    selectFirstSuggestion
} from './../../../functions/common/select';

const caseMutations = element.all(by.css('.case-mutation'));
const form = $('zs-case-object-mutation-form');

describe('when opening case 111', () => {

    beforeAll(() => {

        navigate.as('admin', 111);

    });

    it('there should only be the option to create an object', () => {

        expect(checkObjectButtonPresence('objectmutatie')).toEqual(['aanmaken']);

    });

    describe('and when creating an object mutation of type "create"', () => {

        beforeAll(() => {

            let data = [
                {
                    attribute: form.$('[data-name="objectmutatie_titel"]'),
                    input: ['objectmutatie aanmaken 111']
                },
                {
                    attribute: form.$('[data-name="objectmutatie_tekstveld"]'),
                    input: ['objectmutatie aanmaken 111']
                },
                {
                    attribute: form.$('[data-name="objectmutatie_enkelvoudige_keuze"]'),
                    input: 3
                },
                {
                    attribute: form.$('[data-name="objectmutatie_meervoudige_keuze"]'),
                    input: [ 1, 3 ]
                },
                {
                    attribute: form.$('[data-name="objectmutatie_datum"]'),
                    input: '13-01-2010'
                },
                {
                    attribute: form.$('[data-name="objectmutatie_valuta"]'),
                    input: '12.34'
                },
                {
                    attribute: form.$('[data-name="objectmutatie_adres_dmv_straatnaam"]'),
                    input: ['straat 4']
                }
            ];

            createObjectMutationCreate('objectmutatie', 'aanmaken', data);

        });

        it('the object mutation should be present', () => {

            expect(caseMutations.count()).toEqual(1);

        });

        it('the object mutation tooltip should indicate that it has not been processed yet', () => {

            expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Aan te maken');

        });

        it('the object mutation should have a button for editing', () => {

            expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(true);

        });

        it('the object mutation should not have a button for opening', () => {

            expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(false);

        });

        it('the object mutation should have a button for deleting', () => {

            expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(true);

        });

    });

});

describe('when opening case 112 and creating an object', () => {

    beforeAll(() => {

        navigate.as('admin', 112);

        advance();

        waitForElement('zs-case-result');

        navigate.to(112);

        openPhase(2);

    });

    it('the object mutation should be present', () => {

        expect(caseMutations.count()).toEqual(1);

    });

    it('the object mutation tooltip should indicate that it has been processed', () => {

        expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Aangemaakt');

    });

    it('the object mutation should not have a button for editing', () => {

        expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(false);

    });

    it('the object mutation should have a button for opening', () => {

        expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(true);

    });

    it('the object mutation should not have a button for deleting', () => {

        expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(false);

    });

    // this part of the test has been commented out in anticipation of the new objectView

    // describe('and when opening the object', () => {

    //     beforeAll(() => {

    //         caseMutations.get(0).$('.case-mutation-actions .mdi-launch').click();

    //     });

    //     it('the object should exist and have the correct title', () => {

    //         expect(getTitle()).toEqual('Objectmutatie titel - Create uitvoeren');

    //     });

    //     it('the object should have the given values', () => {

    //         let data = [
    //             'Titel',
    //             'Tekstveld',
    //             'Enkelvoudige keuze',
    //             'Meervoudige keuze',
    //             'Datum',
    //             'Valuta',
    //             'Adres dmv straatnaam'
    //         ],
    //             expectedResults = [
    //             'Create uitvoeren',
    //             'Tekstveld',
    //             'objectmutatie optie 2',
    //             'objectmutatie keuze 1, objectmutatie keuze 3',
    //             '13-01-2017',
    //             '12.34',
    //             'Voerendaal - Florinstraat 1'
    //         ];

    //         expect(getValues(data)).toEqual(expectedResults);

    //     });

    // });

});

describe('when opening case 113', () => {

    beforeAll(() => {

        navigate.as('admin', 113);

    });

    it('there should only be the option to mutate an object', () => {

        expect(checkObjectButtonPresence('objectmutatie')).toEqual(['bewerken']);

    });

    describe('and when creating an object mutation of type "mutate"', () => {

        beforeAll(() => {

            let data = [
                {
                    attribute: form.$('[data-name="objectmutatie_tekstveld"]'),
                    input: ['objectmutatie gewijzigd 113']
                },
                {
                    attribute: form.$('[data-name="objectmutatie_enkelvoudige_keuze"]'),
                    input: 1
                },
                {
                    attribute: form.$('[data-name="objectmutatie_meervoudige_keuze"]'),
                    input: [ 2 ]
                },
                {
                    attribute: form.$('[data-name="objectmutatie_datum"]'),
                    input: '20-04-2015'
                },
                {
                    attribute: form.$('[data-name="objectmutatie_valuta"]'),
                    input: '43,21'
                },
                {
                    attribute: form.$('[data-name="objectmutatie_adres_dmv_straatnaam"]'),
                    input: ['weg 7']
                }
            ];

            createObjectMutationMutate('objectmutatie', 'bewerken', 'Mutate aanmaken', data);

        });

        it('the object mutation should be present', () => {

            expect(caseMutations.count()).toEqual(1);

        });

        it('the object mutation tooltip should indicate that it has not been processed yet', () => {

            expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Te wijzigen');

        });

        it('the object mutation should have a button for editing', () => {

            expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(true);

        });

        it('the object mutation should have a button for opening', () => {

            expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(true);

        });

        it('the object mutation should have a button for deleting', () => {

            expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(true);

        });

    });

});

describe('when opening case 114 and mutating an object', () => {

    beforeAll(() => {

        navigate.as('admin', 114);

        advance();

        waitForElement('zs-case-result');

        navigate.to(114);

        openPhase(2);

    });

    it('the object mutation should be present', () => {

        expect(caseMutations.count()).toEqual(1);

    });

    it('the object mutation tooltip should indicate that it has been processed', () => {

        expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Gewijzigd');

    });

    it('the object mutation should not have a button for editing', () => {

        expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(false);

    });

    it('the object mutation should have a button for opening', () => {

        expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(true);

    });

    it('the object mutation should not have a button for deleting', () => {

        expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(false);

    });

    // this part of the test has been commented out in anticipation of the new objectView

    // describe('and when opening the object', () => {

    //     beforeAll(() => {

    //         caseMutations.get(0).$('.case-mutation-actions .mdi-launch').click();

    //     });

    //     it('the object should exist', () => {

    //         expect(getTitle()).toEqual('Objectmutatie titel - Mutate uitvoeren');

    //     });

    //     it('the object should have the mutated values', () => {

    //         let data = [
    //             'Titel',
    //             'Tekstveld',
    //             'Enkelvoudige keuze',
    //             'Meervoudige keuze',
    //             'Datum',
    //             'Valuta',
    //             'Adres dmv straatnaam'
    //         ],
    //             expectedResults = [
    //             'Mutate uitvoeren',
    //             'Aangepast',
    //             'objectmutatie optie 3',
    //             'objectmutatie keuze 2, objectmutatie keuze 3',
    //             '20-04-2016',
    //             '0,13',
    //             'Klimmen - Achtbunderstraat 2'
    //         ];

    //         expect(getValues(data)).toEqual(expectedResults);

    //     });

    // });

});

describe('when opening case 115', () => {

    beforeAll(() => {

        navigate.as('admin', 115);

    });

    it('there should only be the option to delete an object', () => {

        expect(checkObjectButtonPresence('objectmutatie')).toEqual(['verwijderen']);

    });

    describe('and when creating an object mutation of type "delete"', () => {

        beforeAll(() => {

            createObjectMutationDelete('objectmutatie', 'verwijderen', 'Delete aanmaken');

        });

        it('the object mutation should be present', () => {

            expect(caseMutations.count()).toEqual(1);

        });

        it('the object mutation tooltip should indicate that it has not been processed yet', () => {

            expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Te verwijderen');

        });

        it('the object mutation should not have a button for editing', () => {

            expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(false);

        });

        it('the object mutation should have a button for opening', () => {

            expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(true);

        });

        it('the object mutation should have a button for deleting', () => {

            expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(true);

        });

    });

});

describe('when opening case 116 and deleting an object', () => {

    beforeAll(() => {

        navigate.as('admin', 116);

        advance();

        waitForElement('zs-case-result');

        navigate.to(116);

        openPhase(2);

    });

    it('the object mutation should be present', () => {

        expect(caseMutations.count()).toEqual(1);

    });

    it('the object mutation tooltip should indicate that it has been processed', () => {

        expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Verwijderd');

    });

    it('the object mutation should not have a button for editing', () => {

        expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(false);

    });

    it('the object mutation should not have a button for opening', () => {

        expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(false);

    });

    it('the object mutation should not have a button for deleting', () => {

        expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(false);

    });

    // this part of the test has been commented out in anticipation of the new objectView

    // describe('and when opening the object', () => {

    //     beforeAll(() => {

    //         caseMutations.get(0).$('.case-mutation-actions .mdi-launch').click();

    //     });

    //     it('the object should not exist', () => {

    //         expect(getTitle()).toEqual(null);

    //     });

    // });

});

describe('when opening case 117 and creating an object mutation of type "create" without all the required information', () => {

    beforeAll(() => {

        navigate.as('admin', 117);

        let data = [
            {
                attribute: form.$('[data-name="objectmutatie_titel"]'),
                input: ['objectmutatie aanmaken 117']
            },
            {
                attribute: form.$('[data-name="objectmutatie_enkelvoudige_keuze"]'),
                input: 3
            },
            {
                attribute: form.$('[data-name="objectmutatie_datum"]'),
                input: '13-01-2010'
            },
            {
                attribute: form.$('[data-name="objectmutatie_valuta"]'),
                input: '12.34'
            }
        ];

        createObjectMutationCreate('objectmutatie', 'aanmaken', data);

    });

    it('the object mutation should be present', () => {
    
        expect(caseMutations.count()).toEqual(1);

    });

    it('the object mutation should indicate that it is not complete', () => {

        expect($('.case-mutation .mdi-alert-circle').isDisplayed()).toBe(true);

    });

    it('the case should be unable to advance', () => {

        expect(getAdvanceButtonText()).toEqual('fase incompleet');

    });

    describe('and when completing the object mutation', () => {

        beforeAll(() => {

            let data = [
                {
                    attribute: form.$('[data-name="objectmutatie_tekstveld"]'),
                    input: ['objectmutatie aanmaken 117']
                },
                {
                    attribute: form.$('[data-name="objectmutatie_meervoudige_keuze"]'),
                    input: [ 1, 3 ]
                },
                {
                    attribute: form.$('[data-name="objectmutatie_adres_dmv_straatnaam"]'),
                    input: ['straat 4']
                }
            ];

            editObjectMutation('objectmutatie', 1, data);

        });

        it('the object mutation should be present', () => {
        
            expect(caseMutations.count()).toEqual(1);

        });

        it('the object mutation should indicate that it is not complete', () => {

            expect($('.case-mutation .mdi-alert-circle').isDisplayed()).toBe(false);

        });

        it('the case should be unable to advance', () => {

            expect(getAdvanceButtonText()).toEqual('fase afronden');

        });
        
    });

});

describe('when opening case 118 and creating an object mutation of type "mutate" without all the required information', () => {

    beforeAll(() => {

        navigate.as('admin', 118);

        let data = [
            {
                attribute: form.$('[data-name="objectmutatie_titel"]'),
                input: ['objectmutatie aanmaken 118']
            },
            {
                attribute: form.$('[data-name="objectmutatie_enkelvoudige_keuze"]'),
                input: 3
            },
            {
                attribute: form.$('[data-name="objectmutatie_datum"]'),
                input: '13-01-2010'
            },
            {
                attribute: form.$('[data-name="objectmutatie_valuta"]'),
                input: '12.34'
            }
        ];

        createObjectMutationMutate('objectmutatie', 'bewerken', 'Mutate without info', data);

    });

    it('the object mutation should be present', () => {
    
        expect(caseMutations.count()).toEqual(1);

    });

    xit('the object mutation should indicate that it is not complete', () => {

        expect($('.case-mutation .mdi-alert-circle').isDisplayed()).toBe(true);

    });

    xit('the case should be unable to advance', () => {

        expect(getAdvanceButtonText()).toEqual('fase incompleet');

    });

    describe('and when completing the object mutation', () => {

        beforeAll(() => {

            let data = [
                {
                    attribute: form.$('[data-name="objectmutatie_tekstveld"]'),
                    input: ['objectmutatie aanmaken 118']
                },
                {
                    attribute: form.$('[data-name="objectmutatie_meervoudige_keuze"]'),
                    input: [ 1, 3 ]
                },
                {
                    attribute: form.$('[data-name="objectmutatie_adres_dmv_straatnaam"]'),
                    input: ['straat 4']
                }
            ];

            editObjectMutation('objectmutatie', 1, data);

        });

        it('the object mutation should be present', () => {
        
            expect(caseMutations.count()).toEqual(1);

        });

        it('the object mutation should indicate that it is not complete', () => {

            expect($('.case-mutation .mdi-alert-circle').isDisplayed()).toBe(false);

        });

        it('the case should be unable to advance', () => {

            expect(getAdvanceButtonText()).toEqual('fase afronden');

        });

    });

});

describe('when opening case 119 and starting an object mutation of type "create"', () => {

    beforeAll(() => {

        navigate.as('admin', 119);

        startObjectMutation('objectmutatie', 'aanmaken');

    });

    it('the object mutation should have the values of the case prefilled', () => {

        let data = [
            form.$('[data-name="objectmutatie_titel"]'),
            form.$('[data-name="objectmutatie_tekstveld"]'),
            form.$('[data-name="objectmutatie_enkelvoudige_keuze"]'),
            form.$('[data-name="objectmutatie_meervoudige_keuze"]'),
            form.$('[data-name="objectmutatie_datum"]'),
            form.$('[data-name="objectmutatie_valuta"]'),
            form.$('[data-name="objectmutatie_adres_dmv_straatnaam"]')
        ],
            expectedResults = [
            '',
            'Prefilled',
            'objectmutatie optie 1',
            [ false, true, true ],
            '2016-04-20',
            '0,13',
            'Klimmen - Achtbunderstraat 2'
        ];

        for ( const index in data ) {

            expect(caseAttribute.getValue(data[index])).toEqual(expectedResults[index]);

        }

    });

});

describe('when opening case 120 and starting an object mutation of type "mutate"', () => {

    beforeAll(() => {

        navigate.as('admin', 120);

        startObjectMutation('objectmutatie', 'bewerken');

        selectFirstSuggestion(form.$('vorm-field input'), 'Mutate prefill');

        waitForElement('zs-case-object-mutation-form [data-name="objectmutatie_titel"]');

    });

    it('the object mutation should have the values of the case prefilled', () => {

        let data = [
            form.$('[data-name="objectmutatie_titel"]'),
            form.$('[data-name="objectmutatie_tekstveld"]'),
            form.$('[data-name="objectmutatie_enkelvoudige_keuze"]'),
            form.$('[data-name="objectmutatie_meervoudige_keuze"]'),
            form.$('[data-name="objectmutatie_datum"]'),
            form.$('[data-name="objectmutatie_valuta"]'),
            form.$('[data-name="objectmutatie_adres_dmv_straatnaam"]')
        ],
            expectedResults = [
            'Mutate prefill',
            'Prefilled',
            'objectmutatie optie 1',
            [ false, true, true ],
            '2016-04-20',
            '0,13',
            'Klimmen - Achtbunderstraat 2'
        ];

        for ( const index in data ) {

            expect(caseAttribute.getValue(data[index])).toEqual(expectedResults[index]);

        }

    });

});

describe('when opening case 122 and creating an object mutation of type "mutate" for an object with an existing unprocessed object mutation', () => {

    beforeAll(() => {

        navigate.as('admin', 122);

        createObjectMutationMutate('objectmutatie', 'bewerken', 'Mutate mutated not processed');

    });

    it('the object mutation should not have been created', () => {

        expect(caseMutations.count()).toEqual(0);

    });

});

describe('when opening case 124 and creating an object mutation of type "mutate" for an object with an existing processed object mutation', () => {

    beforeAll(() => {

        navigate.as('admin', 124);

        createObjectMutationMutate('objectmutatie', 'bewerken', 'Mutate mutated processed');

    });

    it('the object mutation should have been created', () => {

        expect(caseMutations.count()).toEqual(1);

    });

});

describe('when opening case 126 and creating an object mutation of type "create" and cancelling it', () => {

    beforeAll(() => {

        navigate.as('admin', 126);

        startObjectMutation('objectmutatie', 'aanmaken');

        performObjectMutationAction('cancel');

    });

    it('the object mutation should not have been created', () => {

        expect(caseMutations.count()).toEqual(0);

    });

});

describe('when opening case 127 and deleting an object mutation', () => {

    beforeAll(() => {

        navigate.as('admin', 127);

        deleteObjectMutation('objectmutatie', 1);

    });

    it('the object mutation should not be present anymore', () => {

        expect(caseMutations.count()).toEqual(0);

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
