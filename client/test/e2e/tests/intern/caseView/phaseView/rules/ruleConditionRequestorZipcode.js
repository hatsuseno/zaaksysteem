import navigate from './../../../../../functions/common/navigate';
import {
    openPhase
} from './../../../../../functions/intern/caseView/caseNav';
import caseAttribute from './../../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const requestorZipcodeSmaller = $('[data-name="aanvrager_postcode_kleiner"]');
const requestorZipcodeExact = $('[data-name="aanvrager_postcode_exact"]');
const requestorZipcodeGreater = $('[data-name="aanvrager_postcode_groter"]');
const requestorZipcodeContains = $('[data-name="aanvrager_postcode_omvat"]');

describe('when opening case 53 with the zipcode requestor', () => {

    beforeAll(() => {

        navigate.as('admin', 53);

        openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(caseAttribute.getClosedValue(requestorZipcodeSmaller)).toEqual('False');
        expect(caseAttribute.getClosedValue(requestorZipcodeExact)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorZipcodeGreater)).toEqual('False');
        expect(caseAttribute.getClosedValue(requestorZipcodeContains)).toEqual('True');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
