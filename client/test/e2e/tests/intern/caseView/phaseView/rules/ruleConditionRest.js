import navigate from './../../../../../functions/common/navigate';
import {
    openPhase
} from './../../../../../functions/intern/caseView/caseNav';
import caseAttribute from './../../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const designationOfConfidentiality = $('[data-name="algemeen_vertrouwelijkheidsaanduiding"]');
const objectionAndAppeal = $('[data-name="algemeen_bezwaar_en_beroep_mogelijk"]');
const publication = $('[data-name="algemeen_publicatie"]');
const bag = $('[data-name="algemeen_bag"]');
const lexSilencioPositivo = $('[data-name="algemeen_lex_silencio_positivo"]');
const suspension = $('[data-name="algemeen_opschorten_mogelijk"]');
const extension = $('[data-name="algemeen_verlengen_mogelijk"]');
const penalty = $('[data-name="algemeen_wet_dwangsom"]');
const wkpb = $('[data-name="algemeen_wkpb"]');

describe('when opening case 62 with all basic casetype settings off', () => {

    beforeAll(() => {

        navigate.as('admin', 62);

        openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(caseAttribute.getClosedValue(designationOfConfidentiality)).toEqual('Geheim');
        expect(caseAttribute.getClosedValue(objectionAndAppeal)).toEqual('True');
        expect(caseAttribute.getClosedValue(publication)).toEqual('True');
        expect(caseAttribute.getClosedValue(bag)).toEqual('True');
        expect(caseAttribute.getClosedValue(lexSilencioPositivo)).toEqual('True');
        expect(caseAttribute.getClosedValue(suspension)).toEqual('True');
        expect(caseAttribute.getClosedValue(extension)).toEqual('True');
        expect(caseAttribute.getClosedValue(penalty)).toEqual('True');
        expect(caseAttribute.getClosedValue(wkpb)).toEqual('True');

    });

});

describe('when opening the case with all basic casetype settings on', () => {

    beforeAll(() => {

        navigate.as('admin', 63);

        openPhase('1');

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(caseAttribute.getClosedValue(designationOfConfidentiality)).toEqual('Uit');
        expect(caseAttribute.getClosedValue(objectionAndAppeal)).toEqual('False');
        expect(caseAttribute.getClosedValue(publication)).toEqual('False');
        expect(caseAttribute.getClosedValue(bag)).toEqual('False');
        expect(caseAttribute.getClosedValue(lexSilencioPositivo)).toEqual('False');
        expect(caseAttribute.getClosedValue(suspension)).toEqual('False');
        expect(caseAttribute.getClosedValue(extension)).toEqual('False');
        expect(caseAttribute.getClosedValue(penalty)).toEqual('False');
        expect(caseAttribute.getClosedValue(wkpb)).toEqual('False');

    });

});


/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
