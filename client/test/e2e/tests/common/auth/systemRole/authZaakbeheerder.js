import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    skipOption
} from './../../../../functions/common/form';

describe('when logging in as behandelaar and open a registration form', () => {

    beforeAll(() => {

        navigate.as('behandelaar');

        let data = {
            casetype: 'Basic casetype',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

    });

    it('should not have the skip required option present', () => {

        expect(skipOption.isPresent()).toBe(false);

    });

});

describe('when logging in as zaakbeheerder and open a registration form', () => {

    beforeAll(() => {

        navigate.as('zaakbeheerder');

        let data = {
            casetype: 'Basic casetype',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

    });

    it('should have the skip required option present', () => {

        expect(skipOption.isPresent()).toBe(true);

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
