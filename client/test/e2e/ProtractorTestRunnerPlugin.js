'use strict'; //eslint-disable-line

let ConfigParser = require('protractor/built/configParser').ConfigParser,
    Runner = require('protractor/built/runner').Runner,
    Plugins = require('protractor/built/plugins').Plugins,
    jasmineRunner = require('protractor/built/frameworks/jasmine.js'),
    path = require('path'),
    once = require('lodash/once'),
    q = require('q'),
    Watchpack = require('watchpack');
    
let watcher = new Watchpack();

let createTestRunner = () => {

    let configParser = new ConfigParser(),
        configFile = path.join(__dirname, 'protractor.config.js'),
        config,
        runner,
        plugins,
        browser,
        next,
        that;

    configParser.addFileConfig(configFile);

    config = configParser.getConfig();

    config.onPrepare = once(config.onPrepare);

    config.specs =
        ConfigParser.resolveFilePatterns(
            ConfigParser.getSpecs(config), false, config.configDir
        );

    runner = new Runner(config);

    plugins = new Plugins(config);

    that = {
        start: once(() => {
            return runner.driverprovider_.setupEnv() //eslint-disable-line
                .then(() => {

                    browser = runner.createBrowser(plugins);
                    return runner.setupGlobals_(browser); //eslint-disable-line
                }).then(() => {
                    return browser.ready.then(browser.getSession);
                });
        }),
        run: ( files ) => {

            if (next) {
                next.resolve();
            }

            // jasminewd needs to be reinitialized every time
            // to ensure expectations are wrapped in webdriver's
            // control flow

            Object.keys(require.cache)
                .filter(key => key.indexOf('jasminewd') !== -1)
                .forEach(key => {
                    delete require.cache[key];
                });

            // clear node require cache to ensure files
            // are re-added to the test suite

            config.specs.forEach(spec => {
                delete require.cache[spec];
            });

            config.specs = files;

            return plugins.setup()
                .then(() => {
                    return jasmineRunner.run(runner, config.specs);
                })
                .catch( err => {
                    console.error(err.stack);
                })
                .then(() => plugins.teardown())
                .then(() => {

                    next = q.defer();

                    browser.wait(next.promise);

                });
        },
        refresh: () => {
            return that.start()
                .then(() => browser.refresh());
        }
    };

    return that;

};

function ProtractorTestRunnerPlugin ( settings ) {
    
    this.settings = settings || {};
}

ProtractorTestRunnerPlugin.prototype.apply = function ( compiler ) {

    let testRunner = createTestRunner(),
        start = q.defer(),
        running = start.promise,
        specs = [];

    let runTests = () => {

        if (!specs.length) {
            return running;
        }

        running = running.then(() => {
            return testRunner.run(specs);
        })
            .catch( ( err ) => {
                console.log('Error calling test runner');
                console.error(err.stack);
            });

        return running;
    };

    compiler.plugin('after-emit', ( comp, cb ) => {

        start.resolve();

        cb();

    });

    running = running.then(testRunner.start);

    watcher.watch(this.settings.files || [], this.settings.directories || [], Date.now() - 10000);

    watcher.on('change', ( filePath ) => {

            specs = [ filePath ];

            runTests();

    });

    process.on('exit', () => {
        watcher.close();
    });


};

module.exports = ProtractorTestRunnerPlugin;

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
