import e2epasswords from './../../e2epasswords.json';

export const loginForm = $('#loginwrap form');
export const usernameField = $('#id_username');
export const passwordField = $('#id_password');
export const submit = $('form input[type=submit]');

export const getPassword = (username) => e2epasswords[username];

export const logout = () => {
    browser.get('/auth/logout');
};

export const login = (username = 'admin', wrongPassword) => {
    const password = wrongPassword ? wrongPassword : getPassword(username);

    usernameField.sendKeys(username);
    passwordField.sendKeys(password);
    submit.click();
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
