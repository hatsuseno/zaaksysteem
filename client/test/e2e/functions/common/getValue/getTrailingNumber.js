const numberEpression = /\s+(\d+)[.!?\s]?$/;

export default string => {
    const number = numberEpression.exec(string);

    return number[1];
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
