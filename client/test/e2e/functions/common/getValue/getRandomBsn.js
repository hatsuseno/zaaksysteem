import randomNumber from 'lodash/random';
import getRandomNumber from './getRandomNumber';

export default ( numberLength ) => {

    let bsnLength = numberLength === undefined ? randomNumber(8, 9) : numberLength,
        bsn,
        i,
        total = 0,
        newNumber,
        lastDigit,
        digits = [];

    do {
        digits = [];
        for ( i = bsnLength; i > 1; i--) {
            newNumber = getRandomNumber(1);
            total += newNumber * i;
            digits.push(newNumber);
        }
    }
    while (total % 11 === 10);

    lastDigit = total % 11;
    digits.push(lastDigit);
    bsn = digits.join('');

    return bsn;

};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
