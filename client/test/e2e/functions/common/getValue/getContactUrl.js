export default ( type, number ) => {

    let contactType;

    switch (type) {
        case 'citizen':
            contactType = 'natuurlijk_persoon';
            break;
        case 'organisation':
            contactType = 'bedrijf';
            break;
        case 'employee':
            contactType = 'medewerker';
            break;
        default:
        break;
    }

    return `/betrokkene/${number}/?gm=1&type=${contactType}`;

};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
