export const selectByText = ( elementToSearchIn, searchValue, resultElements, expectedResult ) => {
    elementToSearchIn.sendKeys(searchValue);

    resultElements
        .filter(result =>
            result
                .getText()
                .then(text =>
                    text === expectedResult
                )
        )
        .click();
};

export const selectFirst = ( elementToSearchIn, searchValue, resultElements ) => {
    elementToSearchIn.sendKeys(searchValue);
    resultElements
        .get(0)
        .click();
};

export const selectFirstSuggestion = ( elementToSearchIn, searchValue ) => {
    const resultElements = element.all(by.css('.suggestion-list-item'));

    selectFirst(elementToSearchIn, searchValue, resultElements);
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
