import angular from 'angular';
import markdownIt from 'markdown-it/dist/markdown-it';
import zsTruncateHtmlModule from './../zsTruncate/zsTruncateHtml';

let parser = markdownIt({
	breaks: true,
	linkify: true
});

export default
	angular.module('zsMarkdown', [
		zsTruncateHtmlModule
	])
		.component('zsMarkdown', {
			bindings: {
				markdown: '<',
				length: '<'
			},
			template: '<zs-truncate-html data-value="$ctrl.getHtml()" data-length="$ctrl.length"></zs-truncate-html>',
			controller: [ '$sce', function ( $sce ) {

				let ctrl = this,
					html = '';

				let resetHtml = ( ) => {

					html = ctrl.markdown ?
						$sce.trustAsHtml(
							parser.render(ctrl.markdown)
						)
						: '';

				};

				ctrl.getHtml = ( ) => html;

				ctrl.$onChanges = ( ) => {
					resetHtml();
				};

				resetHtml();

			}]
		})
		.name;
