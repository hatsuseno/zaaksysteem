import BaseReducer from './../resourceReducer/BaseReducer';

export default ( scope, fn, opts ) => {

	let reducer = new BaseReducer(),
		unwatcher;

	unwatcher = scope.$watch(( ) => fn(), ( value/*, prev*/ ) => {
		if (!reducer.sources || reducer.sources[0] !== value) {
			reducer.setSrc(value);
		}
	}, opts && opts.objectEquality);

	reducer.$setState('resolved');
	reducer.setSrc(fn());

	reducer.onDestroy(( ) => {
		unwatcher();
	});

	return reducer;

};
