import './styles.scss';

export default [
	{
		name: 'public',
		label: 'Openbaar'
	},
	{
		name: 'internal',
		label: 'Intern'
	},
	{
		name: 'confidential',
		label: 'Vertrouwelijk'
	}
];
