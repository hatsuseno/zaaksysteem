import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import template from './template.html';
import './styles.scss';

export default
	angular.module('zsCasePauseApplication', [
		angularUiRouterModule
	])
		.component('zsCasePauseApplication', {
			bindings: {
				pauseApplicationData: '<',
				values: '<'
			},
			controller: [ '$state', '$sce', function ( $state, $sce ) {

				let ctrl = this;

				ctrl.$onChanges = ( ) => {
					ctrl.messageHtml = $sce.trustAsHtml(ctrl.pauseApplicationData.message);
				};

				ctrl.handleResumeClick = ( ) => {

					$state.go(
						$state.current.name,
						{
							casetypeId: ctrl.pauseApplicationData.start_case.prefill.zaaktype.zaaktype_uuid,
							values: ctrl.pauseApplicationData.copy_attributes ?
								ctrl.values
								: null,
							ignoreUnsavedChanges: true
						},
						{ inherit: true }
					);

				};

			}],
			template
		})
		.name;
