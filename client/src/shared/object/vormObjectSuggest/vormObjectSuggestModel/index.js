import angular from 'angular';
import template from './template.html';
import zsObjectSuggestModule from './../../zsObjectSuggest';
import vormObjectSuggestDisplayModule from './../vormObjectSuggestDisplay';
import get from 'lodash/get';

export default
	angular.module('vormObjectSuggestModel', [
		zsObjectSuggestModule,
		vormObjectSuggestDisplayModule
	])
		.directive('vormObjectSuggestModel', [ 'vormInvoke', ( ) => {
			
			return {
				restrict: 'E',
				template,
				scope: {
					delegate: '&',
					inputId: '&',
					placeholder: '@'
				},
				require: [ 'vormObjectSuggestModel', '^vormField', 'ngModel' ],
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this,
						vormField,
						ngModel;

					ctrl.link = ( controllers ) => {
						[ vormField, ngModel ] = controllers;
					};

					ctrl.getObjectType = ( ) => {
						return vormField ? vormField.invokeData('objectType') : null;
					};

					ctrl.handleSuggest = ( $object ) => {

						let formatter = get(vormField.templateData(), 'format'),
							object = $object;

						if (formatter) {
							object = formatter(object);
						}

						ngModel.$setViewValue(object, 'click');
					};

					ctrl.clearObject = ( ) => {
						vormField.clearDelegate(ctrl.delegate());
					};

					ctrl.getObject = ( ) => ngModel.$modelValue;

				}],
				controllerAs: 'vormObjectSuggestModel',
				link: ( scope, element, attrs, controllers ) => {
					controllers.shift().link(controllers);
				}
			};

		}])
		.name;
