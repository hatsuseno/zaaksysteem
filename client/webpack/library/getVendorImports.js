/**
 * Recursively scan an app entry point for ES2015 imports of installed npm modules.
 */
const detective = require('detective-es6');
const validateNpmPackageName = require('validate-npm-package-name');
const { dirname, extname, join, resolve } = require('path');
const { existsSync, lstatSync, readFileSync } = require('fs');
const getModuleBaseName = require('./getModuleBaseName');

const BASE_PATH = resolve(process.cwd(), '..', 'client', 'src');

const isDirectory = pathAbsolute =>
  (existsSync(pathAbsolute) && lstatSync(pathAbsolute).isDirectory());

const coreModulesExceptions = ['url'];

const isNpmModule = modulePath => (
  coreModulesExceptions.includes(modulePath)
  || validateNpmPackageName(getModuleBaseName(modulePath))
    .validForNewPackages
);

function readFile( filePath ) {
  if (isDirectory(filePath)) {
    return readFileSync(`${filePath}/index.js`, 'utf-8');
  }

  if (extname(filePath)) {
    return readFileSync(filePath, 'utf-8');
  }

  return readFileSync(`${filePath}.js`, 'utf-8');
}

const getImports = filePath =>
  detective(readFile(filePath));

function getFileName( relativePath ) {
  if (extname(relativePath)) {
    return relativePath;
  }

  if (isDirectory(join(BASE_PATH, relativePath))) {
    return `${relativePath.replace(/\/$/, '')}/index.js`;
  }

  return `${relativePath}.js`;
}

/**
 * @param {string} modulePath
 * @param {Array} [vendors=[]]
 * @return {Array}
 */
function getVendorImports( modulePath, exclusions = [], vendors = [] ) {
  const modulePathAbsolute = join(BASE_PATH, modulePath);

  if (!exclusions.includes(modulePathAbsolute)) {
    const imports = getImports(modulePathAbsolute);

    exclusions.push(modulePathAbsolute);

    for (const subModulePath of imports) {
      if (isNpmModule(subModulePath)) {
        if (!vendors.includes(subModulePath)) {
          vendors.push(subModulePath);
        }
      } else {
        const relativePath = join(
          isDirectory(modulePathAbsolute) ? modulePath : dirname(modulePath),
          subModulePath
        );
        const fileName = getFileName(relativePath);

        if (!vendors.includes(fileName)) {
          const extension = extname(relativePath);

          if (!extension || extension === '.js') {
            getVendorImports(relativePath, exclusions, vendors);
          }
        }
      }
    }
  }

  return {
    vendors,
    exclusions,
  };
}

module.exports = getVendorImports;
