# webpack

## Build vendors bundle

    $ npm run build-vendors

## Build app bundles

### Production build

- compress JS 
- mangle JS indentifiers
- minimize CSS


    $ npm run build-apps
    
### Production build for debugging

- without service workers
- without JS identifier mangling
- with JS source map
- with CSS source map


    $ npm run build-apps -- --env debug

## Development

    $ npm start
